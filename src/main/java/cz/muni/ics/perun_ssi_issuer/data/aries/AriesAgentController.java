package cz.muni.ics.perun_ssi_issuer.data.aries;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.CredDefAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.SchemaAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaDefinitionEntity;
import lombok.NonNull;
import org.hyperledger.acy_py.generated.model.V20CredExRecordDetail;
import org.hyperledger.aries.api.connection.ConnectionRecord;
import org.hyperledger.aries.api.connection.CreateInvitationResponse;
import org.hyperledger.aries.api.issue_credential_v2.V20CredExRecord;
import org.hyperledger.aries.api.present_proof_v2.V20PresExRecord;

public interface AriesAgentController {

    CreateInvitationResponse createInvitation();

    ConnectionRecord getConnection(@NonNull String connectionId);

    V20CredExRecord sendCredential(@NonNull String credentialDefinitionId,
                                   @NonNull String connectionId,
                                   @NonNull String schemaId,
                                   @NonNull String comment,
                                   @NonNull JsonNode userAttributes);

    V20CredExRecordDetail getCredentialExchange(String credentialExchangeId);

    String createCredentialDefinition(@NonNull SchemaDefinitionEntity schemaDefinition,
                                      boolean supportRevocation,
                                      Integer revocationRegistrySize) throws CredDefAlreadyExistsException;

    String createSchema(@NonNull SchemaDefinitionEntity definition) throws SchemaAlreadyExistsException;

    V20PresExRecord createPresentationExchange(@NonNull PresentationSchemaDefinitionEntity schema,
                                               @NonNull ConnectionEntity connection);

    V20PresExRecord getPresentationExchange(@NonNull String presExchId);
}
