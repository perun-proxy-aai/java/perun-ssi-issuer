package cz.muni.ics.perun_ssi_issuer.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.hyperledger.aries.webhook.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static cz.muni.ics.perun_ssi_issuer.web.controller.WebhookEndpointController.PATH_WEBHOOK_BASE;

@Slf4j
@Controller
@RequestMapping(PATH_WEBHOOK_BASE)
public class WebhookEndpointController {

    // === CONSTANTS === //

    public static final String PATH_WEBHOOK_BASE = "/webhook";

    // === CLASS FIELDS === //

    private final EventHandler handler;

    // === CONSTRUCTORS === //

    @Autowired
    public WebhookEndpointController(EventHandler handler) {
        this.handler = handler;
    }

    // === PUBLIC METHODS === //

    @PostMapping("/topics/{topic}/")
    public ResponseEntity<Object> webhookTopics(@PathVariable String topic,
                                                @RequestBody String payload) {
        log.info("Received webhook call of topic '{}' with body: '{}'", topic, payload);
        handler.handleEvent(topic, payload);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/topic/{topic}/")
    public ResponseEntity<Object> webhookTopic(@PathVariable String topic,
                                               @RequestBody String payload) {
        log.info("Received webhook call of topic '{}' with body: '{}'", topic, payload);
        handler.handleEvent(topic, payload);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
