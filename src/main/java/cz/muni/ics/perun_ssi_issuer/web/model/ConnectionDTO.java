package cz.muni.ics.perun_ssi_issuer.web.model;

import cz.muni.ics.perun_ssi_issuer.common.enums.ConnectionStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class ConnectionDTO {

    @NotNull
    private Long id;

    @NotBlank
    private String alias;

    @NotNull
    private ConnectionStatus status;

    @NotNull
    private List<CredentialDTO> activeCredentials;

    @NotNull
    private List<CredentialDTO> revokedCredentials;

    @NotNull
    private List<CredentialDTO> unprocessedCredentials;

    @NotNull
    private List<CredentialDefinitionDTO> availableCredentials;

}
