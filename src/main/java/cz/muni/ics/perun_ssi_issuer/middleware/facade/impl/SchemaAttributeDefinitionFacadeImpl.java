package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaAttributeDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.SchemaAttributeDefinitionRepository;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.SchemaAttributeDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.SchemaAttributeDefinitionForm;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;

@Component
public class SchemaAttributeDefinitionFacadeImpl implements SchemaAttributeDefinitionFacade {

    private final SchemaAttributeDefinitionRepository attributeDefinitionRepository;

    @Autowired
    public SchemaAttributeDefinitionFacadeImpl(
            @NonNull SchemaAttributeDefinitionRepository attributeDefinitionRepository
    ) {
        this.attributeDefinitionRepository = attributeDefinitionRepository;
    }

    @Override
    public List<SchemaAttributeDefinitionEntity> getDefinitions() {
        return attributeDefinitionRepository.findAll();
    }

    @Override
    public SchemaAttributeDefinitionEntity getDefinition(@NonNull Long schAttrDefId)
            throws SchemaAttributeDefinitionNotFoundException {
        return attributeDefinitionRepository
                .findById(schAttrDefId)
                .orElseThrow(SchemaAttributeDefinitionNotFoundException::new);
    }

    @Override
    public SchemaAttributeDefinitionEntity createDefinition(@NonNull SchemaAttributeDefinitionForm form) {
        SchemaAttributeDefinitionEntity definition = SchemaAttributeDefinitionEntity.builder()
                .name(form.getName())
                .perunAttribute(form.getRpcName())
                .mimeType(form.getMimeType())
                .schemaDefinitions(new HashSet<>())
                .attributeType(form.getType())
                .build();
        return attributeDefinitionRepository.save(definition);
    }

    @Transactional
    @Override
    public SchemaAttributeDefinitionEntity updateDefinition(
            @NonNull SchemaAttributeDefinitionEntity oldEntity,
            @NonNull SchemaAttributeDefinitionForm form
    ) {
        oldEntity.setName(form.getName());
        oldEntity.setPerunAttribute(form.getRpcName());
        oldEntity.setMimeType(form.getMimeType());
        oldEntity.setAttributeType(form.getType());
        return attributeDefinitionRepository.save(oldEntity);
    }

    @Override
    public void deleteDefinition(@NonNull SchemaAttributeDefinitionEntity definition) {
        attributeDefinitionRepository.delete(definition);
    }

}
