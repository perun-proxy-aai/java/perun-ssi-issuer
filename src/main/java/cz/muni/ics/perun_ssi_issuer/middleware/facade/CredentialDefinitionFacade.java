package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.CredDefAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.ConnectionCredentials;
import lombok.NonNull;

import java.util.List;

public interface CredentialDefinitionFacade {

    List<CredentialDefinitionEntity> getAvailableCredentials(
            @NonNull ConnectionCredentials credentials);

    CredentialDefinitionEntity getCredentialDefinition(@NonNull Long id)
            throws CredentialDefinitionNotFoundException;

    List<CredentialDefinitionEntity> getDefinitions();

    CredentialDefinitionEntity createCredentialDefinition(@NonNull Long schemaDefId,
                                                          boolean supportRevocation,
                                                          Integer revocationRegistrySize)
            throws SchemaDefinitionNotFoundException, CredDefAlreadyExistsException;

    void deleteCredentialDefinition(@NonNull Long id) throws CredentialDefinitionNotFoundException;

}
