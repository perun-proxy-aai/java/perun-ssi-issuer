package cz.muni.ics.perun_ssi_issuer.common.exception;

public class UnauthorizedPresentationExchangeAccessException extends Exception {

    public UnauthorizedPresentationExchangeAccessException() {
        super();
    }

    public UnauthorizedPresentationExchangeAccessException(String message) {
        super(message);
    }

    public UnauthorizedPresentationExchangeAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedPresentationExchangeAccessException(Throwable cause) {
        super(cause);
    }

    protected UnauthorizedPresentationExchangeAccessException(String message, Throwable cause,
                                                              boolean enableSuppression,
                                                              boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
