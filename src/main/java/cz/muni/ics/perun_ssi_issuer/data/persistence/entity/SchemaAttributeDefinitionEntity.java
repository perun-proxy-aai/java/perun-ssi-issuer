package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import cz.muni.ics.perun_ssi_issuer.common.enums.SchemaAttributeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
//DB
@Entity(name = "SchemaAttributeDefinition")
@Table(
        name = "schema_attribute_definitions",
        indexes = @Index(name = "idx_sch_attr_defs_name", columnList = "name",
                unique = true),
        uniqueConstraints = @UniqueConstraint(name = "constr_unique_sch_attr_defs_name",
                columnNames = "name")
)
public class SchemaAttributeDefinitionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "mime_type")
    private String mimeType;

    @NotNull
    @Column(name = "attribute_type", nullable = false)
    private SchemaAttributeType attributeType;

    @NotBlank
    @Column(name = "perun_attribute", nullable = false)
    private String perunAttribute;

    @NotNull
    @ManyToMany(mappedBy = "attributeDefinitions")
    @ToString.Exclude
    @Builder.Default
    private Set<SchemaDefinitionEntity> schemaDefinitions = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        SchemaAttributeDefinitionEntity that = (SchemaAttributeDefinitionEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public void addSchemaDefinition(@NonNull SchemaDefinitionEntity definition) {
        schemaDefinitions.add(definition);
        definition.getAttributeDefinitions().add(this);
    }

    public void removeSchemaDefinition(@NonNull SchemaDefinitionEntity definition) {
        schemaDefinitions.remove(definition);
        definition.getAttributeDefinitions().remove(this);
    }

}

