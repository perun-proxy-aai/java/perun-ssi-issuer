package cz.muni.ics.perun_ssi_issuer.data.perun.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.StringUtils;

/**
 * Attribute definition from Perun.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>;
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class AttributeDefinition {

    public static final String BEAN_NAME = "AttributeDefinition";

    public final static String STRING_TYPE = "java.lang.String";
    public final static String INTEGER_TYPE = "java.lang.Integer";
    public final static String BOOLEAN_TYPE = "java.lang.Boolean";
    public final static String ARRAY_TYPE = "java.util.ArrayList";
    public final static String MAP_TYPE = "java.util.LinkedHashMap";

    @NonNull
    private Long id;

    @NonNull
    private String friendlyName;

    @NonNull
    private String namespace;

    @NonNull
    private String type;

    @NonNull
    private String entity;

    public AttributeDefinition(@NonNull Long id,
                               @NonNull String friendlyName,
                               @NonNull String namespace,
                               @NonNull String type,
                               @NonNull String entity) {
        this.setId(id);
        this.setFriendlyName(friendlyName);
        this.setNamespace(namespace);
        this.setType(type);
        this.setEntity(entity);
    }

    public void setFriendlyName(@NonNull String friendlyName) {
        if (!StringUtils.hasText(friendlyName)) {
            throw new IllegalArgumentException("FriendlyName cannot be empty!");
        }
        this.friendlyName = friendlyName;
    }

    public void setNamespace(@NonNull String namespace) {
        if (!StringUtils.hasText(namespace)) {
            throw new IllegalArgumentException("Namespace cannot be empty!");
        }
        this.namespace = namespace;
    }

    public void setType(@NonNull String type) {
        if (!StringUtils.hasText(type)) {
            throw new IllegalArgumentException("Type cannot be empty!");
        }
        this.type = type;
    }

    public void setEntity(@NonNull String entity) {
        if (!StringUtils.hasText(entity)) {
            throw new IllegalArgumentException("Entity cannot be empty!");
        }
        this.entity = entity;
    }

    @JsonIgnore
    public String getFullName() {
        return this.namespace + ':' + this.friendlyName;
    }

}
