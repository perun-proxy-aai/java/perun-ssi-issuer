package cz.muni.ics.perun_ssi_issuer.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public class ApiKeyAuthenticationProcessingFilter extends AbstractPreAuthenticatedProcessingFilter {

    public static final String HEADER_API_KEY = "x-api-key";

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        return "N/A";
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        return request.getHeader(HEADER_API_KEY);
    }

}
