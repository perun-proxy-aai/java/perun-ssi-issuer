package cz.muni.ics.perun_ssi_issuer.data.persistence.repository;

import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaAttributeDefinitionEntity;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface SchemaAttributeDefinitionRepository
        extends JpaRepository<SchemaAttributeDefinitionEntity, Long> {
    Optional<SchemaAttributeDefinitionEntity> findByName(@NonNull String name);

    Collection<SchemaAttributeDefinitionEntity> findAllByNameIn(@NonNull Collection<String> names);

    Collection<SchemaAttributeDefinitionEntity> findAllByIdIn(@NonNull Collection<Long> ids);

}
