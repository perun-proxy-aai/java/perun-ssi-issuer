package cz.muni.ics.perun_ssi_issuer.middleware.service.properties;

import cz.muni.ics.perun_ssi_issuer.common.enums.SchemaAttributeType;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class SchemaAttributeDefinition {

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    private String mimeType;

    @NotNull
    private SchemaAttributeType valueType;

    @NotBlank
    private String perunRpcAttribute;

}
