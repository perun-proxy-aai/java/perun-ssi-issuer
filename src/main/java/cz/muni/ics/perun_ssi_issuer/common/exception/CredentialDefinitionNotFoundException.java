package cz.muni.ics.perun_ssi_issuer.common.exception;

public class CredentialDefinitionNotFoundException extends Exception {

    public CredentialDefinitionNotFoundException() {
        super();
    }

    public CredentialDefinitionNotFoundException(String message) {
        super(message);
    }

    public CredentialDefinitionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public CredentialDefinitionNotFoundException(Throwable cause) {
        super(cause);
    }

    protected CredentialDefinitionNotFoundException(String message, Throwable cause, boolean enableSuppression,
                                                    boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
