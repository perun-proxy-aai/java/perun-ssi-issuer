package cz.muni.ics.perun_ssi_issuer.data.perun.adapter;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Getter
@Slf4j
@AllArgsConstructor

@Validated
@ConstructorBinding
@ConfigurationProperties(prefix = "perun.adapter")
public class PerunAdapterProperties implements InitializingBean {

    @NotBlank
    private final String extSourceName;

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("Initialized '{}' properties", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

}
