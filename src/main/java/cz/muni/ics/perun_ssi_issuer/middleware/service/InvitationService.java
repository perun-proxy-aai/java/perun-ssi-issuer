package cz.muni.ics.perun_ssi_issuer.middleware.service;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.InvitationNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.QRCodeGeneratorError;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedInvitationAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.web.model.ConnectionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.InvitationDTO;
import lombok.NonNull;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface InvitationService {

    InvitationDTO createInvitation(@NonNull String alias,
                                   @NonNull Authentication authentication)
            throws UserNotFoundException, QRCodeGeneratorError, InvitationNotFoundException,
            NoUserDetailsAvailableException;

    InvitationDTO getConnectionInvitation(@NonNull Long id, @NonNull Authentication authentication)
            throws QRCodeGeneratorError, InvitationNotFoundException, NoUserDetailsAvailableException,
            UnauthorizedInvitationAccessException;

    List<InvitationDTO> getInvitations(@NonNull Authentication authentication)
            throws QRCodeGeneratorError, NoUserDetailsAvailableException;


    ConnectionDTO getConnectionForProcessedInvitation(@NonNull Long invitationId,
                                                      @NonNull Authentication authentication)
            throws ConnectionNotFoundException, InvitationNotFoundException,
            NoUserDetailsAvailableException, UnauthorizedInvitationAccessException,
            UnauthorizedConnectionAccessException;

    void cancelInvitation(@NonNull Long invitationId,
                          @NonNull Authentication authentication)
            throws NoUserDetailsAvailableException, InvitationNotFoundException,
            UnauthorizedInvitationAccessException;

    InvitationDTO getConnectionInvitationIfProcessed(@NonNull Long invitationId,
                                                     @NonNull Authentication authentication)
            throws InvitationNotFoundException, NoUserDetailsAvailableException, QRCodeGeneratorError,
            UnauthorizedInvitationAccessException;
}
