package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserIdentifierAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserUpdateException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.UserEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.UserFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.service.UserService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.EntityMappingUtils;
import cz.muni.ics.perun_ssi_issuer.web.model.UserDetailsDTO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserFacade userFacade;

    @Autowired
    public UserServiceImpl(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Override
    public UserDetailsDTO saveOrUpdateUserDetails(@NonNull Authentication auth)
            throws NoUserIdentifierAvailableException, NoUserDetailsAvailableException,
            UserUpdateException {
        if (!(auth.getPrincipal() instanceof OidcUser)) {
            log.warn("Logged in user is not instance of OidcUser");
            throw new NoUserDetailsAvailableException("No processable user details available");
        }
        OidcUser user = (OidcUser) auth.getPrincipal();
        String userIdentifier = user.getSubject();
        if (!StringUtils.hasText(userIdentifier)) {
            log.error("No userIdentifier available for details '{}'", user);
            throw new NoUserIdentifierAvailableException("No userIdentifier available");
        }
        UserEntity userEntity = userFacade.saveOrUpdate(user, userIdentifier);
        return EntityMappingUtils.map(userEntity);
    }

    // === PRIVATE METHODS === //

}
