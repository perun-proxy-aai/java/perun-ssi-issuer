package cz.muni.ics.perun_ssi_issuer.data.aries.exception;

import lombok.Getter;

@Getter
public class SchemaAlreadyExistsException extends Exception {

    private final String existingId;

    public SchemaAlreadyExistsException(String existingId) {
        super();
        this.existingId = existingId;
    }

    public SchemaAlreadyExistsException(String message, String existingId) {
        super(message);
        this.existingId = existingId;
    }

    public SchemaAlreadyExistsException(String message, Throwable cause, String existingId) {
        super(message, cause);
        this.existingId = existingId;
    }

    public SchemaAlreadyExistsException(Throwable cause, String existingId) {
        super(cause);
        this.existingId = existingId;
    }

    protected SchemaAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String existingId) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.existingId = existingId;
    }
}
