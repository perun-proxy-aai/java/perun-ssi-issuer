package cz.muni.ics.perun_ssi_issuer.middleware.service;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.web.model.ConnectionDTO;
import lombok.NonNull;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface ConnectionService {

    ConnectionDTO getConnection(@NonNull Long id,
                                @NonNull Authentication auth)
            throws UnauthorizedConnectionAccessException, ConnectionNotFoundException,
            DatabaseInconsistencyException, NoUserDetailsAvailableException;

    List<ConnectionDTO> getConnections(@NonNull Authentication auth)
            throws UserNotFoundException, NoUserDetailsAvailableException;

}
