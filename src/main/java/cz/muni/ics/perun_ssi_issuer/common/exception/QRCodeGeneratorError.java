package cz.muni.ics.perun_ssi_issuer.common.exception;

public class QRCodeGeneratorError extends Exception {

    public QRCodeGeneratorError() {
        super();
    }

    public QRCodeGeneratorError(String message) {
        super(message);
    }

    public QRCodeGeneratorError(String message, Throwable cause) {
        super(message, cause);
    }

    public QRCodeGeneratorError(Throwable cause) {
        super(cause);
    }

    protected QRCodeGeneratorError(String message,
                                   Throwable cause,
                                   boolean enableSuppression,
                                   boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
