package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.TranslationEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.TranslationRepository;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.TranslationFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.Translations;
import lombok.NonNull;
import org.apache.commons.lang3.function.TriFunction;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class TranslationFacadeImpl implements TranslationFacade {

    private final TranslationRepository repository;

    public TranslationFacadeImpl(TranslationRepository repository) {
        this.repository = repository;
    }

    @Override
    public void createSchemaAttributeDefinitionTranslations(@NonNull Translations translations,
                                                            @NonNull String definitionName)
    {
        createTranslations(translations, TranslationFacade.getSchAttrDefI18nDefBase(definitionName));
    }

    @Override
    public void updateSchemaAttributeDefinitionTranslations(@Valid @NonNull Translations translations,
                                                            @NonNull String definitionName) {
        updateSchemaAttributeDefinitionTranslationsInternal(translations, definitionName, null);
    }

    @Override
    public void updateSchemaAttributeDefinitionTranslations(@Valid @NonNull Translations translations,
                                                            @NonNull String definitionName,
                                                            @NonNull String oldName) {
        updateSchemaAttributeDefinitionTranslationsInternal(translations, definitionName, oldName);
    }

    @Override
    public void deleteSchemaAttributeDefinitionTranslations(@NonNull String definitionName) {
        deleteTranslations(TranslationFacade.getSchAttrDefI18nDefBase(definitionName));
    }

    @Override
    public Translations getSchemaAttributeDefinitionTranslations(@NonNull String definitionName) {
        return getTranslations(TranslationFacade.getSchAttrDefI18nDefBase(definitionName));
    }

    @Override
    public void createSchemaDefinitionTranslations(@NonNull Translations translations,
                                                   @NonNull String definitionName)
    {
        createTranslations(translations, TranslationFacade.getSchDefI18nDefBase(definitionName));
    }

    @Override
    public void updateSchemaDefinitionTranslations(@Valid @NonNull Translations translations,
                                                   @NonNull String definitionName) {
        updateSchemaDefinitionTranslationsInternal(translations, definitionName, null);
    }

    @Override
    public void updateSchemaDefinitionTranslations(@Valid @NonNull Translations translations,
                                                   @NonNull String definitionName,
                                                   @NonNull String oldName) {
        updateSchemaDefinitionTranslationsInternal(translations, definitionName, oldName);
    }

    @Override
    public void deleteSchemaDefinitionTranslations(@NonNull String definitionName) {
        deleteTranslations(TranslationFacade.getSchDefI18nDefBase(definitionName));
    }

    @Override
    public Translations getSchemaDefinitionTranslations(@NonNull String definitionName) {
        return getTranslations(TranslationFacade.getSchDefI18nDefBase(definitionName));
    }

    @Override
    public void createPresentationSchemaAttributeDefinitionTranslations(@NonNull Translations translations,
                                                                        @NonNull String definitionName)
    {
        createTranslations(translations, TranslationFacade.getPresSchAttrDefI18nDefBase(definitionName));
    }

    @Override
    public void updatePresentationSchemaAttributeDefinitionTranslations(@Valid @NonNull Translations translations,
                                                                        @NonNull String definitionName)
    {
        updatePresentationSchemaAttributeDefinitionTranslationsInternal(translations, definitionName, null);
    }

    @Override
    public void updatePresentationSchemaAttributeDefinitionTranslations(@Valid @NonNull Translations translations,
                                                                        @NonNull String definitionName,
                                                                        @NonNull String oldName)
    {
        updatePresentationSchemaAttributeDefinitionTranslationsInternal(translations, definitionName, oldName);
    }

    @Override
    public void deletePresentationSchemaAttributeDefinitionTranslations(@NonNull String definitionName) {
        deleteTranslations(TranslationFacade.getPresSchAttrDefI18nDefBase(definitionName));
    }

    @Override
    public Translations getPresentationSchemaAttributeDefinitionTranslations(@NonNull String definitionName) {
        return getTranslations(TranslationFacade.getPresSchAttrDefI18nDefBase(definitionName));
    }

    @Override
    public void createPresentationSchemaDefinitionTranslations(@NonNull Translations translations,
                                                               @NonNull String definitionName)
    {
        createTranslations(translations, TranslationFacade.getPresSchDefI18nDefBase(definitionName));
    }

    @Override
    public void updatePresentationSchemaDefinitionTranslations(@Valid @NonNull Translations translations,
                                                               @NonNull String definitionName)
    {
        updatePresentationSchemaDefinitionTranslationsInternal(translations, definitionName, null);
    }

    @Override
    public void updatePresentationSchemaDefinitionTranslations(@Valid @NonNull Translations translations,
                                                               @NonNull String definitionName,
                                                               @NonNull String oldName)
    {
        updatePresentationSchemaDefinitionTranslationsInternal(translations, definitionName, oldName);
    }

    @Override
    public void deletePresentationSchemaDefinitionTranslations(@NonNull String definitionName) {
        deleteTranslations(TranslationFacade.getPresSchDefI18nDefBase(definitionName));
    }

    @Override
    public Translations getPresentationSchemaDefinitionTranslations(@NonNull String definitionName) {
        return getTranslations(TranslationFacade.getPresSchDefI18nDefBase(definitionName));
    }

    // === PROTECTED METHODS === //

    @Transactional
    protected void updateSchemaAttributeDefinitionTranslationsInternal(@Valid @NonNull Translations translations,
                                                                       @NonNull String definitionName,
                                                                       String oldName)
    {
        if (StringUtils.hasText(oldName) && !definitionName.equals(oldName)) {
            deleteTranslations(TranslationFacade.getSchAttrDefI18nDefBase(oldName));
        }
        updateTranslations(translations, TranslationFacade.getSchAttrDefI18nDefBase(definitionName));
    }

    @Transactional
    protected void updateSchemaDefinitionTranslationsInternal(@Valid @NonNull Translations translations,
                                                              @NonNull String definitionName,
                                                              String oldName)
    {
        if (StringUtils.hasText(oldName) && !definitionName.equals(oldName)) {
            deleteTranslations(TranslationFacade.getSchDefI18nDefBase(oldName));
        }
        updateTranslations(translations, TranslationFacade.getSchDefI18nDefBase(definitionName));
    }

    @Transactional
    protected void updatePresentationSchemaAttributeDefinitionTranslationsInternal(
            @Valid @NonNull Translations translations,
            @NonNull String definitionName,
            String oldName)
    {
        if (StringUtils.hasText(oldName) && !definitionName.equals(oldName)) {
            deleteTranslations(TranslationFacade.getPresSchAttrDefI18nDefBase(oldName));
        }
        updateTranslations(translations, TranslationFacade.getPresSchAttrDefI18nDefBase(definitionName));
    }

    @Transactional
    protected void updatePresentationSchemaDefinitionTranslationsInternal(@Valid @NonNull Translations translations,
                                                                          @NonNull String definitionName,
                                                                          String oldName)
    {
        if (StringUtils.hasText(oldName) && !definitionName.equals(oldName)) {
            deleteTranslations(TranslationFacade.getPresSchDefI18nDefBase(oldName));
        }
        updateTranslations(translations, TranslationFacade.getPresSchDefI18nDefBase(definitionName));
    }

    @Transactional
    protected void createTranslations(@NonNull Translations translations, @NonNull String key) {
        List<TranslationEntity> translationList = new ArrayList<>();
        translationList.addAll(
                createTranslations(
                        TranslationFacade.getNameTranslationKey(key),
                        translations.getNameTranslations(),
                        this::buildTranslation
                )
        );
        translationList.addAll(
                createTranslations(
                        TranslationFacade.getDescTranslationKey(key),
                        translations.getDescTranslations(),
                        this::buildTranslation
                )
        );
        repository.saveAll(translationList);
    }

    @Transactional
    protected void updateTranslations(@NonNull Translations translations, @NonNull String key) {
        List<TranslationEntity> updatedTranslations = new ArrayList<>();
        List<TranslationEntity> deletedTranslations = new ArrayList<>();

        processUpdateNames(key,
                translations.getNameTranslations(), updatedTranslations, deletedTranslations);
        processUpdateDescriptions(key,
                translations.getDescTranslations(), updatedTranslations, deletedTranslations);

        if (!updatedTranslations.isEmpty()) {
            repository.saveAll(updatedTranslations);
        }
        if (!deletedTranslations.isEmpty()) {
            repository.deleteAll(deletedTranslations);
        }
    }

    @Transactional
    protected void deleteTranslations(@NonNull String translationKeyBase) {
        repository.deleteAllByMsgKey(TranslationFacade.getNameTranslationKey(translationKeyBase));
        repository.deleteAllByMsgKey(TranslationFacade.getDescTranslationKey(translationKeyBase));
    }

    private Translations getTranslations(@NonNull String key) {
        List<TranslationEntity> nameTranslations = repository.findAllByMsgKey(
                TranslationFacade.getNameTranslationKey(key)
        );
        List<TranslationEntity> descTranslations = repository.findAllByMsgKey(
                TranslationFacade.getDescTranslationKey(key)
        );

        Map<String, String> mappedNameTranslations = new HashMap<>();
        for (TranslationEntity entity : nameTranslations) {
            mappedNameTranslations.put(entity.getLocale(), entity.getContent());
        }
        Map<String, String> mappedDescTranslations = new HashMap<>();
        for (TranslationEntity entity : descTranslations) {
            mappedDescTranslations.put(entity.getLocale(), entity.getContent());
        }
        return new Translations(mappedNameTranslations, mappedDescTranslations);
    }

    // === PRIVATE METHODS === //

    private void processUpdateNames(@NonNull String key,
                                    @NonNull Map<String, String> translations,
                                    @NonNull List<TranslationEntity> updatedTranslations,
                                    @NonNull List<TranslationEntity> deletedTranslations) {
        processUpdate(
                TranslationFacade.getNameTranslationKey(key),
                translations,
                this::buildTranslation,
                updatedTranslations,
                deletedTranslations
        );
    }

    private void processUpdateDescriptions(@NonNull String key,
                                           @NonNull Map<String, String> translations,
                                           @NonNull List<TranslationEntity> updatedTranslations,
                                           @NonNull List<TranslationEntity> deletedTranslations) {
        processUpdate(
                TranslationFacade.getDescTranslationKey(key),
                translations,
                this::buildTranslation,
                updatedTranslations,
                deletedTranslations
        );
    }

    private void processUpdate(@NonNull String key,
                               @NonNull Map<String, String> translations,
                               @NonNull TriFunction<String, String, String, TranslationEntity> createEntityFunction,
                               @NonNull List<TranslationEntity> updatedTranslations,
                               @NonNull List<TranslationEntity> deletedTranslations)
    {
        Set<String> langs = new HashSet<>(translations.keySet());
        List<TranslationEntity> ts = repository.findAllByMsgKey(key);
        Map<String, TranslationEntity> descsMap = convertToMap(ts);
        langs.addAll(descsMap.keySet());

        for (String lang : langs) {
            String translation = translations.getOrDefault(lang, null);
            TranslationEntity t = descsMap.getOrDefault(lang, null);
            if (translation != null) {
                if (t != null) {
                    t.setContent(translation);
                } else {
                    t = createEntityFunction.apply(lang, translation, key);
                }
                updatedTranslations.add(t);
            } else {
                if (t != null) {
                    deletedTranslations.add(t);
                }
            }
        }
    }

    private List<TranslationEntity> createTranslations(
            @NonNull String key,
            @NonNull Map<String, String> translations,
            @NonNull TriFunction<String, String, String, TranslationEntity> createFunction) {
        List<TranslationEntity> translationEntities = new ArrayList<>();
        for (String lang : translations.keySet()) {
            translationEntities.add(
                    createFunction.apply(lang, translations.get(lang), key)
            );
        }
        return translationEntities;
    }

    private TranslationEntity buildTranslation(@NonNull String lang,
                                               @NonNull String content,
                                               @NonNull String key) {
        return TranslationEntity.builder()
                .locale(lang)
                .msgKey(key)
                .content(content)
                .build();
    }

    private Map<String, TranslationEntity> convertToMap(
            @NonNull List<TranslationEntity> translations) {
        Map<String, TranslationEntity> langToObjectMap = new HashMap<>();
        for (TranslationEntity t : translations) {
            langToObjectMap.put(t.getLocale(), t);
        }
        return langToObjectMap;
    }

}
