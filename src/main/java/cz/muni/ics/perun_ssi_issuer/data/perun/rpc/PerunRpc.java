package cz.muni.ics.perun_ssi_issuer.data.perun.rpc;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Map;

public interface PerunRpc {

    JsonNode call(String manager, String method, Map<String, Object> map);

}
