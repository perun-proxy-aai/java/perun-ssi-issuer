package cz.muni.ics.perun_ssi_issuer.web.validation;

import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Map;

public class LocalizationMapValidator implements ConstraintValidator<LocalizationMapConstraint, Map<String, String>> {

    @Override
    public boolean isValid(Map<String, String> value, ConstraintValidatorContext context) {
        for (String v : value.values()) {
            if (!StringUtils.hasText(v)) {
                return false;
            }
        }
        return true;
    }

}
