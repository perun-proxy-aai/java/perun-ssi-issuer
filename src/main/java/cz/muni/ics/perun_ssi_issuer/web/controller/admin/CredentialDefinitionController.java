package cz.muni.ics.perun_ssi_issuer.web.controller.admin;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.CredDefAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.middleware.service.CredentialDefinitionService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.CredentialService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.SchemaDefinitionService;
import cz.muni.ics.perun_ssi_issuer.web.controller.AppController;
import cz.muni.ics.perun_ssi_issuer.web.model.CredentialDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.CredentialDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.CredentialDefinitionForm;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.CREDENTIAL_DEF_ID;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CREDENTIAL_DEFINITIONS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CREDENTIAL_DEFINITIONS_CREATE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CREDENTIAL_DEFINITIONS_DELETE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CREDENTIAL_DEFINITIONS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_CREDENTIAL_DEFINITIONS;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_CREDENTIAL_DEFINITIONS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_CREDENTIAL_DEFINITIONS_FORM;

@Controller
@Slf4j
public class CredentialDefinitionController extends AppController {

    public static final String MODEL_ATTR_CREDENTIAL_DEF_FORM = "form";

    public static final String MODEL_ATTR_CREDENTIAL_DEF_ID = "credentialDefId";

    public static final String MODEL_ATTR_CREDENTIAL_DEFINITIONS = "definitions";

    public static final String MODEL_ATTR_CREDENTIAL_DEFINITION = "definition";

    public static final String MODEL_ATTR_DELETE_FAILED = "deleteFailed";

    public static final String MODEL_ATTR_SCHEMA_DEFINITIONS = "schemaDefinitions";

    public static final String MODEL_ATTR_CREDENTIALS = "credentials";

    public static final String MODEL_ATTR_CRED_DEF_EXISTS = "credDefExists";

    public static final String MODEL_ATTR_SCHEMA_NOT_FOUND = "schemaNotFound";

    private final CredentialDefinitionService service;

    private final SchemaDefinitionService schemaDefinitionService;

    private final CredentialService credentialService;

    @Autowired
    public CredentialDefinitionController(
            @NonNull ApplicationProperties applicationProperties,
            @NonNull OidcProperties oidcProperties,
            CredentialDefinitionService credentialDefinitionService,
            SchemaDefinitionService schemaDefinitionService, CredentialService credentialService) {
        super(applicationProperties, oidcProperties);
        this.service = credentialDefinitionService;
        this.schemaDefinitionService = schemaDefinitionService;
        this.credentialService = credentialService;
    }


    @GetMapping(PATH_CREDENTIAL_DEFINITIONS)
    public String credentialDefinitions(Model model) {
        List<CredentialDefinitionDTO> credentialDefinitions = service.getDefinitions();
        model.addAttribute(MODEL_ATTR_CREDENTIAL_DEFINITIONS, credentialDefinitions);
        return VIEW_CREDENTIAL_DEFINITIONS;
    }

    @GetMapping(PATH_CREDENTIAL_DEFINITIONS_DETAIL)
    public String credentialDefinition(@PathVariable(CREDENTIAL_DEF_ID) Long credentialDefId,
                                       Model model)
            throws CredentialDefinitionNotFoundException {
        CredentialDefinitionDTO credentialDefinition = service.getDefinition(credentialDefId);
        model.addAttribute(MODEL_ATTR_CREDENTIAL_DEFINITION, credentialDefinition);
        List<CredentialDTO> credentials = credentialService.getCredentialsByDefinitionId(credentialDefId);
        model.addAttribute(MODEL_ATTR_CREDENTIALS, credentials);
        return VIEW_CREDENTIAL_DEFINITIONS_DETAIL;
    }

    @GetMapping(PATH_CREDENTIAL_DEFINITIONS_CREATE)
    public String createDefinition(@ModelAttribute(MODEL_ATTR_CREDENTIAL_DEF_FORM)
                                   CredentialDefinitionForm form,
                                   Model model) {
        model.addAttribute(MODEL_ATTR_CREDENTIAL_DEF_ID, null);
        model.addAttribute(MODEL_ATTR_SCHEMA_DEFINITIONS, schemaDefinitionService.getDefinitions());
        return VIEW_CREDENTIAL_DEFINITIONS_FORM;
    }

    @PostMapping(PATH_CREDENTIAL_DEFINITIONS_CREATE)
    public String createDefinitionSubmit(@Valid @ModelAttribute(MODEL_ATTR_CREDENTIAL_DEF_FORM)
                                         CredentialDefinitionForm form,
                                         BindingResult bindingResult,
                                         RedirectAttributes redirectAttributes,
                                         Model model)
    {
        model.addAttribute(MODEL_ATTR_SCHEMA_DEFINITIONS, schemaDefinitionService.getDefinitions());
        if (bindingResult.hasErrors()) {
            return VIEW_CREDENTIAL_DEFINITIONS_FORM;
        }
        CredentialDefinitionDTO credentialDefinition;
        try {
            credentialDefinition = service.createDefinition(form);
        } catch (CredDefAlreadyExistsException ex) {
            model.addAttribute(MODEL_ATTR_CRED_DEF_EXISTS, ex.getExistingId());
            return VIEW_CREDENTIAL_DEFINITIONS_FORM;
        } catch (SchemaDefinitionNotFoundException e) {
            model.addAttribute(MODEL_ATTR_SCHEMA_NOT_FOUND, true);
            return VIEW_CREDENTIAL_DEFINITIONS_FORM;
        }
        redirectAttributes.addAttribute(CREDENTIAL_DEF_ID, credentialDefinition.getId());
        return "redirect:" + PATH_CREDENTIAL_DEFINITIONS_DETAIL;
    }

    @PostMapping(PATH_CREDENTIAL_DEFINITIONS_DELETE)
    public String deleteDefinition(@PathVariable(CREDENTIAL_DEF_ID) Long credentialDefId,
                                   Model model)
            throws CredentialDefinitionNotFoundException {
        boolean success = service.deleteDefinition(credentialDefId);
        if (!success) {
            model.addAttribute(CREDENTIAL_DEF_ID, credentialDefId);
            model.addAttribute(MODEL_ATTR_DELETE_FAILED, true);
            return "redirect:" + PATH_CREDENTIAL_DEFINITIONS_DETAIL;
        } else {
            return "redirect:" + PATH_CREDENTIAL_DEFINITIONS;
        }
    }

}
