package cz.muni.ics.perun_ssi_issuer.data.perun.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import cz.muni.ics.perun_ssi_issuer.data.perun.exceptions.InconvertibleValueException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Representation of attribute from Perun.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>;
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class Attribute extends AttributeDefinition {

    public static final String BEAN_NAME = "Attribute";

    private JsonNode value;

    private Timestamp valueCreatedAt;

    private Timestamp valueModifiedAt;

    public Attribute(@NonNull AttributeDefinition def,
                     JsonNode value,
                     Timestamp valueCreatedAt,
                     Timestamp valueModifiedAt) {
        this(def.getId(),
                def.getFriendlyName(),
                def.getNamespace(),
                def.getType(),
                def.getEntity(),
                value,
                valueCreatedAt,
                valueModifiedAt
        );
    }

    public Attribute(@NonNull Long id,
                     @NonNull String friendlyName,
                     @NonNull String namespace,
                     @NonNull String type,
                     @NonNull String entity,
                     JsonNode value,
                     Timestamp valueCreatedAt,
                     Timestamp valueModifiedAt) {
        super(id, friendlyName, namespace, type, entity);
        this.setValue(value);
        this.setValueCreatedAt(valueCreatedAt);
        this.setValueModifiedAt(valueModifiedAt);
    }

    public static boolean isEmptyValue(JsonNode value) {
        if (value == null || value instanceof NullNode) {
            return true;
        } else if (value.isNull()) {
            return true;
        } else if (value.isTextual()) {
            String v = value.asText();
            return v == null || "null".equalsIgnoreCase(v) || !StringUtils.hasText(v);
        } else if (value.isBoolean()) {
            return false;
        } else if (value.isArray() || value.isObject() || value.isContainerNode()) {
            return value.isEmpty();
        }
        return false;
    }

    public void setValue(JsonNode value) {
        this.value = resolveValue(value);
    }

    private JsonNode resolveValue(JsonNode value) {
        if (isEmptyValue(value)) {
            switch (getType()) {
                case BOOLEAN_TYPE:
                    return JsonNodeFactory.instance.booleanNode(false);
                case STRING_TYPE:
                    return JsonNodeFactory.instance.textNode("");
                case INTEGER_TYPE:
                    return JsonNodeFactory.instance.nullNode();
                case ARRAY_TYPE:
                    return JsonNodeFactory.instance.arrayNode();
                case MAP_TYPE:
                    return JsonNodeFactory.instance.objectNode();
            }
        }
        return value;
    }

    public String valueAsString() {
        if (STRING_TYPE.equals(getType())) {
            if (value == null || value instanceof NullNode) {
                return null;
            } else if (value instanceof TextNode) {
                return value.textValue();
            }
        }

        return value.asText();
    }

    public Long valueAsLong() {
        if (INTEGER_TYPE.equals(getType())) {
            if (isEmptyValue(value)) {
                return null;
            } else if (value instanceof NumericNode) {
                return value.longValue();
            }
        }

        throw inconvertible(Long.class.getName());
    }

    public boolean valueAsBoolean() {
        if (BOOLEAN_TYPE.equals(getType())) {
            if (value == null || value instanceof NullNode) {
                return false;
            } else if (value instanceof BooleanNode) {
                return value.asBoolean();
            }
        }

        throw inconvertible(Boolean.class.getName());
    }

    public List<String> valueAsList() {
        List<String> arr = new ArrayList<>();
        if (ARRAY_TYPE.equals(getType())) {
            if (isEmptyValue(value)) {
                return new ArrayList<>();
            } else if (value instanceof ArrayNode) {
                ArrayNode arrJson = (ArrayNode) value;
                arrJson.forEach(item -> arr.add(item.asText()));
            }
        } else {
            arr.add(valueAsString());
        }

        return arr;
    }

    public Map<String, String> valueAsMap() throws InconvertibleValueException {
        if (MAP_TYPE.equals(getType())) {
            if (isEmptyValue(value)) {
                return new HashMap<>();
            } else if (value instanceof ObjectNode) {
                Map<String, String> res = new HashMap<>();
                ObjectNode objJson = (ObjectNode) value;
                Iterator<String> it = objJson.fieldNames();
                while (it.hasNext()) {
                    String key = it.next();
                    res.put(key, objJson.get(key).asText());
                }
                return res;
            }
        }

        throw inconvertible(Map.class.getName());
    }

    public boolean isEmptyValue() {
        return isEmptyValue(this.value);
    }

    private InconvertibleValueException inconvertible(String clazzName) {
        return new InconvertibleValueException("Cannot convert value of attribute to " + clazzName +
                " for object: " + this);
    }

}
