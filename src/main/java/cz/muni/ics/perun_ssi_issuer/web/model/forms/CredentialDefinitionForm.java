package cz.muni.ics.perun_ssi_issuer.web.model.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class CredentialDefinitionForm {

    @NotNull
    private Long schemaDefinition;

    private boolean supportRevocation;

    private Integer revocationRegistrySize;

}
