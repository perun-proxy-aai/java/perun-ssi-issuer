package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
//DB
@Entity(name = "PresentationExchange")
@Table(
        name = "presentation_exchanges",
        indexes = @Index(name = "idx_pres_exch", columnList = "presentation_exchange_id",
                unique = true),
        uniqueConstraints = @UniqueConstraint(name = "constr_unique_pres_exch_id",
                columnNames = "presentation_exchange_id")
)
public class PresentationExchangeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "presentation_exchange_id", nullable = false)
    @NotNull
    private String presentationExchangeId;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "connection_id", nullable = false)
    @ToString.Exclude
    private ConnectionEntity connection;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schema_id", nullable = false)
    @ToString.Exclude
    private PresentationSchemaDefinitionEntity schema;

    @Nullable
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cred_exch_content_id")
    @ToString.Exclude
    private PresentationExchangeContentEntity content;

    @Column(name = "completed", nullable = false)
    boolean completed = false;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        PresentationExchangeEntity that = (PresentationExchangeEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}

