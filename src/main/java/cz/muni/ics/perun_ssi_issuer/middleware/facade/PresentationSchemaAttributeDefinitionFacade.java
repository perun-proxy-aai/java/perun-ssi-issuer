package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationSchemaAttributeDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentationSchemaAttributeDefinitionForm;
import lombok.NonNull;

import java.util.List;

public interface PresentationSchemaAttributeDefinitionFacade {
    List<PresentationSchemaAttributeDefinitionEntity> getDefinitions();

    PresentationSchemaAttributeDefinitionEntity getDefinition(@NonNull Long id) throws PresentationSchemaAttributeDefinitionNotFoundException;

    PresentationSchemaAttributeDefinitionEntity createDefinition(@NonNull PresentationSchemaAttributeDefinitionForm form);

    PresentationSchemaAttributeDefinitionEntity updateDefinition(@NonNull PresentationSchemaAttributeDefinitionEntity oldEntity,
                                                                 @NonNull PresentationSchemaAttributeDefinitionForm form);

    void deleteDefinition(@NonNull PresentationSchemaAttributeDefinitionEntity definition);

}
