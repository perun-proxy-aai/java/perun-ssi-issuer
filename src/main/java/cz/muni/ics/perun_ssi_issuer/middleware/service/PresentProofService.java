package cz.muni.ics.perun_ssi_issuer.middleware.service;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationExchangeNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedPresentationExchangeAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.web.model.ConnectionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationExchangeDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentProofForm;
import lombok.NonNull;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface PresentProofService {

    List<ConnectionDTO> getUserConnections(@NonNull Authentication auth)
            throws NoUserDetailsAvailableException, UserNotFoundException;

    List<PresentationSchemaDefinitionDTO> getSchemasForProofs();


    PresentationExchangeDTO createPresentationExchange(@NonNull PresentProofForm proofSelectionForm,
                                                       @NonNull Authentication auth)
            throws NoUserDetailsAvailableException, ConnectionNotFoundException, SchemaDefinitionNotFoundException,
            UnauthorizedConnectionAccessException;


    boolean checkPresentationExchangeStatus(@NonNull Long presExchId, @NonNull Authentication auth)
            throws NoUserDetailsAvailableException, UnauthorizedPresentationExchangeAccessException,
            PresentationExchangeNotFoundException, DatabaseInconsistencyException;
}
