package cz.muni.ics.perun_ssi_issuer.middleware.service;

import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.SchemaAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.web.model.SchemaDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.SchemaDefinitionForm;
import lombok.NonNull;

import javax.transaction.Transactional;
import java.util.List;

public interface SchemaDefinitionService {

    List<SchemaDefinitionDTO> getDefinitions();

    SchemaDefinitionDTO getDefinition(@NonNull Long schemaDefId) throws SchemaDefinitionNotFoundException;

    SchemaDefinitionForm getDefinitionForEdit(@NonNull Long id) throws SchemaDefinitionNotFoundException;

    @Transactional
    SchemaDefinitionDTO createDefinition(@NonNull SchemaDefinitionForm form) throws SchemaAlreadyExistsException;

    @Transactional
    SchemaDefinitionDTO updateDefinition(@NonNull Long id, @NonNull SchemaDefinitionForm form)
            throws SchemaDefinitionNotFoundException;

    @Transactional
    boolean deleteDefinition(@NonNull Long schemaDefId) throws SchemaDefinitionNotFoundException;

}
