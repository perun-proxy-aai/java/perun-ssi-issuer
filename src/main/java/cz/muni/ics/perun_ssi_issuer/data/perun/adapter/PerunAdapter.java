package cz.muni.ics.perun_ssi_issuer.data.perun.adapter;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.perun_ssi_issuer.data.perun.exceptions.PerunUserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.User;
import lombok.NonNull;

import java.util.Map;
import java.util.Set;

public interface PerunAdapter {

    User identifyPerunUser(@NonNull String userIdentifier) throws PerunUserNotFoundException;

    Map<String, JsonNode> getUserAttributes(@NonNull User user,
                                            @NonNull Set<String> attributesToFetch);

}
