package cz.muni.ics.perun_ssi_issuer.data.persistence.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;

@Slf4j
@Converter
public class JsonNodeConverter implements AttributeConverter<JsonNode, String> {

    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(JsonNode attribute) {
        try {
            if (attribute != null && !attribute.isNull()) {
                return OBJECT_MAPPER.writeValueAsString(attribute);
            }
        } catch (JsonProcessingException e) {
            log.error("JSON writing error", e);
        }
        return null;
    }

    @Override
    public JsonNode convertToEntityAttribute(String dbData) {
        try {
            return OBJECT_MAPPER.readValue(dbData, JsonNode.class);
        } catch (final IOException e) {
            log.error("JSON reading error", e);
        }
        return JsonNodeFactory.instance.nullNode();
    }

}
