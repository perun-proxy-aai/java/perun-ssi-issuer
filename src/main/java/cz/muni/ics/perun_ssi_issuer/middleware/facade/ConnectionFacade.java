package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.ConnectionCredentials;
import lombok.NonNull;

import java.util.List;

public interface ConnectionFacade {

    ConnectionEntity getConnection(@NonNull Long id, @NonNull String userIdentifier)
            throws ConnectionNotFoundException, DatabaseInconsistencyException,
            UnauthorizedConnectionAccessException;


    ConnectionCredentials getConnectionCredentials(@NonNull ConnectionEntity connection);

    List<ConnectionEntity> getActiveUserConnections(@NonNull String userIdentifier) throws UserNotFoundException;
}
