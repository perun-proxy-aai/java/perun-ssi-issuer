package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
//DB
@Validated
@Entity(name = "Invitation")
@Table(
        name = "invitations",
        indexes = @Index(name = "idx_invitations_connection_id", columnList = "connection_id",
                unique = true),
        uniqueConstraints = @UniqueConstraint(name = "constr_unique_invitations_connection_id",
                columnNames = "connection_id")
)
public class InvitationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "connection_id", nullable = false, unique = true)
    private String connectionId;

    @NotBlank
    @Column(name = "alias", nullable = false)
    private String alias;

    @NotBlank
    @Column(name = "link", nullable = false, columnDefinition = "TEXT")
    private String link;

    @Column(name = "pending", nullable = false)
    private boolean pending;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @ToString.Exclude
    private UserEntity user;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        InvitationEntity that = (InvitationEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}

