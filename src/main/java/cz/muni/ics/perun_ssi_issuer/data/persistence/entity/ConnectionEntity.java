package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import cz.muni.ics.perun_ssi_issuer.common.enums.ConnectionStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
//DB
@Entity(name = "Connection")
@Table(
        name = "connections",
        indexes = @Index(name = "idx_connections_connection_id", columnList = "connection_id",
                unique = true),
        uniqueConstraints = @UniqueConstraint(name = "constr_unique_connections_connection_id",
                columnNames = "connection_id")
)
public class ConnectionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "connection_id", nullable = false)
    private String connectionId;

    @NotBlank
    @Column(name = "alias", nullable = false)
    private String alias;

    @Column(name = "status", nullable = false)
    private ConnectionStatus status;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "connection")
    @ToString.Exclude
    @Builder.Default
    private List<CredentialEntity> credentials = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @ToString.Exclude
    private UserEntity user;

    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        ConnectionEntity that = (ConnectionEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public void addCredential(CredentialEntity credential) {
        this.credentials.add(credential);
    }

    public void removeCredential(CredentialEntity credential) {
        this.credentials.remove(credential);
    }

}

