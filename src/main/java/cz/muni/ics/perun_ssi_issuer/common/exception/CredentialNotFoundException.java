package cz.muni.ics.perun_ssi_issuer.common.exception;

public class CredentialNotFoundException extends Exception {

    public CredentialNotFoundException() {
        super();
    }

    public CredentialNotFoundException(String message) {
        super(message);
    }

    public CredentialNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public CredentialNotFoundException(Throwable cause) {
        super(cause);
    }

    protected CredentialNotFoundException(String message, Throwable cause, boolean enableSuppression,
                                          boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
