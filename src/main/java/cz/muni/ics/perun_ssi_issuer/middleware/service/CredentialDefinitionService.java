package cz.muni.ics.perun_ssi_issuer.middleware.service;

import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.CredDefAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.web.model.CredentialDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.CredentialDefinitionForm;
import lombok.NonNull;

import javax.transaction.Transactional;
import java.util.List;

public interface CredentialDefinitionService {

    List<CredentialDefinitionDTO> getDefinitions();

    CredentialDefinitionDTO getDefinition(@NonNull Long credentialDefId)
            throws CredentialDefinitionNotFoundException;

    @Transactional
    CredentialDefinitionDTO createDefinition(@NonNull CredentialDefinitionForm form)
            throws SchemaDefinitionNotFoundException, CredDefAlreadyExistsException;

    @Transactional
    boolean deleteDefinition(@NonNull Long credentialDefId)
            throws CredentialDefinitionNotFoundException;

}
