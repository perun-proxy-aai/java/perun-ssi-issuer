package cz.muni.ics.perun_ssi_issuer.data.perun.rpc;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Getter
@Slf4j

@Validated
@ConstructorBinding
@ConfigurationProperties(prefix = "perun.connector.rpc")
public class PerunRpcProperties implements InitializingBean {

    @NotBlank
    private final String url;
    @NotBlank
    @ToString.Exclude
    private final String username;
    @NotBlank
    private final String password;
    private final String serializer;
    @Min(1)
    private final int connectionTimeout;
    @Min(1)
    private final int connectionRequestTimeout;
    @Min(1)
    private final int requestTimeout;

    public PerunRpcProperties(String url,
                              String username,
                              String password,
                              String serializer,
                              Integer connectionTimeout,
                              Integer connectionRequestTimeout,
                              Integer requestTimeout) {
        if (!StringUtils.hasText(serializer)) {
            serializer = "jsonlite";
        }
        this.url = url;
        this.username = username;
        this.password = password;
        this.serializer = serializer;
        this.connectionTimeout = Objects.requireNonNullElse(connectionTimeout, 30000);
        this.connectionRequestTimeout = Objects.requireNonNullElse(connectionRequestTimeout, 30000);
        this.requestTimeout = Objects.requireNonNullElse(requestTimeout, 60000);
    }

    @Override
    public void afterPropertiesSet() {
        log.info("Initialized '{}' properties", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

}
