package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
//DB
@Entity(name = "CredentialDefinition")
@Table(
        name = "credential_definitions",
        indexes = @Index(name = "idx_cred_defs_cred_def_id", columnList = "credential_definition_id",
                unique = true),
        uniqueConstraints = @UniqueConstraint(name = "constr_unique_cred_defs_cred_def_id",
                columnNames = "credential_definition_id")
)
public class CredentialDefinitionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "credential_definition_id", nullable = false)
    private String credentialDefinitionId;

    @Builder.Default
    @Column(name = "support_revocation", nullable = false)
    private boolean supportRevocation = true;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schema_id", nullable = false)
    @ToString.Exclude
    private SchemaDefinitionEntity schemaDefinition;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        CredentialDefinitionEntity that = (CredentialDefinitionEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}

