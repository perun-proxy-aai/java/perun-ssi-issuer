package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationSchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentationSchemaDefinitionForm;
import lombok.NonNull;

import java.util.List;
import java.util.Set;

public interface PresentationSchemaDefinitionFacade {
    List<PresentationSchemaDefinitionEntity> getDefinitions();

    PresentationSchemaDefinitionEntity getDefinition(@NonNull Long id)
            throws PresentationSchemaDefinitionNotFoundException;

    PresentationSchemaDefinitionEntity createDefinition(@NonNull PresentationSchemaDefinitionForm form,
                                                        @NonNull Set<Long> attributeDefinitions);

    void deleteDefinition(@NonNull PresentationSchemaDefinitionEntity definition);
}
