package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationSchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.PresentationSchemaAttributeDefinitionRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.PresentationSchemaDefinitionRepository;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.PresentationSchemaDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentationSchemaDefinitionForm;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PresentationSchemaDefinitionFacadeImpl implements PresentationSchemaDefinitionFacade {

    private final PresentationSchemaDefinitionRepository repository;

    private final PresentationSchemaAttributeDefinitionRepository attributeDefinitionRepository;

    @Autowired
    public PresentationSchemaDefinitionFacadeImpl(@NonNull PresentationSchemaDefinitionRepository repository,
                                                  @NonNull PresentationSchemaAttributeDefinitionRepository attributeDefinitionRepository)
    {
        this.repository = repository;
        this.attributeDefinitionRepository = attributeDefinitionRepository;
    }

    @Override
    public List<PresentationSchemaDefinitionEntity> getDefinitions() {
        return repository.findAll();
    }

    @Override
    public PresentationSchemaDefinitionEntity getDefinition(@NonNull Long id)
            throws PresentationSchemaDefinitionNotFoundException
    {
        return repository.findById(id).orElseThrow(PresentationSchemaDefinitionNotFoundException::new);
    }

    @Override
    public PresentationSchemaDefinitionEntity createDefinition(
            @NonNull PresentationSchemaDefinitionForm form,
            @NonNull Set<Long> attributeDefinitions
    ) {
        Set<PresentationSchemaAttributeDefinitionEntity> attributes = new HashSet<>(
                attributeDefinitionRepository.findAllByIdIn(attributeDefinitions)
        );
        PresentationSchemaDefinitionEntity entity = PresentationSchemaDefinitionEntity.builder()
                .name(form.getName())
                .schemaDefinitionId(form.getSchemaId())
                .build();
        entity.setAttributeDefinitions(attributes);
        return repository.save(entity);
    }

    @Override
    public void deleteDefinition(@NonNull PresentationSchemaDefinitionEntity definition) {
        Set<PresentationSchemaAttributeDefinitionEntity> attributes = definition.getAttributeDefinitions();
        for (PresentationSchemaAttributeDefinitionEntity attr : attributes) {
            attr.removeSchemaDefinition(definition);
        }
        attributeDefinitionRepository.saveAll(attributes);
        repository.delete(definition);
    }

}
