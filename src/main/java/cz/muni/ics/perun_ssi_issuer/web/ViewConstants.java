package cz.muni.ics.perun_ssi_issuer.web;

public interface ViewConstants {

    // == ERROR VIEWS == //
    String VIEW_GENERAL_ERROR = "errors/general_error";

    String VIEW_BAD_REQUEST = "errors/bad_request";

    String VIEW_UNAUTHORIZED = "errors/unauthorized";

    String VIEW_FORBIDDEN = "errors/forbidden";

    String VIEW_NOT_FOUND = "errors/not_found";

    // == AUTH VIEWS == //

    String VIEW_LOGIN = "login/login";

    String VIEW_LOGIN_ERROR = "login/login_error";

    String VIEW_LOGOUT = "logout/logout";

    String VIEW_LOGOUT_COMPLETE = "logout/logout_complete";

    // == REGULAR VIEWS == //

    String VIEW_CONNECTIONS = "connections";

    String VIEW_CONNECTION_DETAIL = "connection_detail";

    String VIEW_INVITATION_CREATE = "invitation_create";

    String VIEW_INVITATION_DETAIL = "invitation_detail";

    String VIEW_INVITATION_PROCESSED = "invitation_processed";

    String VIEW_CREDENTIAL_ISSUE = "credential_issue";

    String VIEW_CREDENTIAL_PROCESSED = "credential_processed";

    String VIEW_CREDENTIAL_DETAIL = "credential_detail";

    String VIEW_PRESENT_PROOF_SELECTION = "present_proof_selection";

    String VIEW_PRESENT_PROOF_PROMPT = "present_proof_prompt";

    String VIEW_PRESENT_PROOF_PROMPT_SUCCESS = "present_proof_prompt_success";

    // === ADMIN VIEWS === //

    String VIEWS_ADMIN = "admin/";

    String VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS = VIEWS_ADMIN + "schema_attr_defs";

    String VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL = VIEWS_ADMIN + "schema_attr_def_detail";

    String VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM = VIEWS_ADMIN + "schema_attr_def_form";

    String VIEW_SCHEMA_DEFINITIONS = VIEWS_ADMIN + "schema_defs";

    String VIEW_SCHEMA_DEFINITIONS_DETAIL = VIEWS_ADMIN + "schema_def_detail";

    String VIEW_SCHEMA_DEFINITIONS_FORM = VIEWS_ADMIN + "schema_def_form";

    String VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS = VIEWS_ADMIN + "pres_schema_attr_defs";

    String VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL = VIEWS_ADMIN + "pres_schema_attr_def_detail";

    String VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM = VIEWS_ADMIN + "pres_schema_attr_def_form";

    String VIEW_PRES_SCHEMA_DEFINITIONS = VIEWS_ADMIN + "pres_schema_defs";

    String VIEW_PRES_SCHEMA_DEFINITIONS_DETAIL = VIEWS_ADMIN + "pres_schema_def_detail";

    String VIEW_PRES_SCHEMA_DEFINITIONS_FORM = VIEWS_ADMIN + "pres_schema_def_form";

    String VIEW_CREDENTIAL_DEFINITIONS = VIEWS_ADMIN + "credential_defs";

    String VIEW_CREDENTIAL_DEFINITIONS_DETAIL = VIEWS_ADMIN + "credential_def_detail";

    String VIEW_CREDENTIAL_DEFINITIONS_FORM = VIEWS_ADMIN + "credential_def_form";

    String VIEW_ADMIN = VIEWS_ADMIN + "index";

}
