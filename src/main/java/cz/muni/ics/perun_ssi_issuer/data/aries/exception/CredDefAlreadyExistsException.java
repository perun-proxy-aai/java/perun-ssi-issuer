package cz.muni.ics.perun_ssi_issuer.data.aries.exception;

import lombok.Getter;

@Getter
public class CredDefAlreadyExistsException extends Exception {

    private final String existingId;

    public CredDefAlreadyExistsException(String existingId) {
        super();
        this.existingId = existingId;
    }

    public CredDefAlreadyExistsException(String message, String existingId) {
        super(message);
        this.existingId = existingId;
    }

    public CredDefAlreadyExistsException(String message, Throwable cause, String existingId) {
        super(message, cause);
        this.existingId = existingId;
    }

    public CredDefAlreadyExistsException(Throwable cause, String existingId) {
        super(cause);
        this.existingId = existingId;
    }

    protected CredDefAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String existingId) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.existingId = existingId;
    }
}
