package cz.muni.ics.perun_ssi_issuer.web.model;

import cz.muni.ics.perun_ssi_issuer.web.model.references.ConnectionReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class CredentialDTO {

    @NotNull
    private Long id;

    @NotNull
    private Map<String, String> attributes;

    private boolean processed;

    private boolean accepted;

    private boolean active;

    @NotNull
    private ConnectionReference connectionReference;

    @NotNull
    private CredentialDefinitionDTO credentialDefinition;

}
