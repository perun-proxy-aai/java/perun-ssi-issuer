package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.UserEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.ConnectionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.CredentialDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.UserFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.ConnectionCredentials;
import cz.muni.ics.perun_ssi_issuer.middleware.service.ConnectionService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.EntityMappingUtils;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.ServiceUtils;
import cz.muni.ics.perun_ssi_issuer.web.model.ConnectionDTO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ConnectionServiceImpl implements ConnectionService {

    private final UserFacade userFacade;

    private final ConnectionFacade connectionFacade;

    private final CredentialDefinitionFacade credentialDefinitionFacade;

    @Autowired
    public ConnectionServiceImpl(UserFacade userFacade,
                                 ConnectionFacade connectionFacade,
                                 CredentialDefinitionFacade credentialDefinitionFacade) {
        this.userFacade = userFacade;
        this.connectionFacade = connectionFacade;
        this.credentialDefinitionFacade = credentialDefinitionFacade;
    }

    @Override
    public List<ConnectionDTO> getConnections(@NonNull Authentication auth)
            throws UserNotFoundException, NoUserDetailsAvailableException {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        UserEntity user = userFacade.getUser(userIdentifier);
        if (user.getConnections() == null || user.getConnections().isEmpty()) {
            return new ArrayList<>();
        }

        return EntityMappingUtils.mapConnections(user.getConnections());
    }

    @Override
    public ConnectionDTO getConnection(@NonNull Long id,
                                       @NonNull Authentication auth)
            throws ConnectionNotFoundException, DatabaseInconsistencyException,
            NoUserDetailsAvailableException, UnauthorizedConnectionAccessException {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        ConnectionEntity connection = connectionFacade.getConnection(id, userIdentifier);
        ConnectionCredentials credentials = connectionFacade.getConnectionCredentials(connection);
        List<CredentialDefinitionEntity> availableCredentials = credentialDefinitionFacade
                .getAvailableCredentials(credentials);

        return ConnectionDTO.builder()
                .id(connection.getId())
                .alias(connection.getAlias())
                .activeCredentials(EntityMappingUtils.mapCredentials(credentials.getActive()))
                .revokedCredentials(EntityMappingUtils.mapCredentials(credentials.getRevoked()))
                .unprocessedCredentials(EntityMappingUtils.mapCredentials(credentials.getUnprocessed()))
                .availableCredentials(EntityMappingUtils.mapCredentialDefinitions(availableCredentials))
                .status(connection.getStatus())
                .build();
    }

}
