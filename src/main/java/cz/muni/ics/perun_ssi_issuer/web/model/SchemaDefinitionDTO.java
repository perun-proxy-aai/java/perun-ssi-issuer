package cz.muni.ics.perun_ssi_issuer.web.model;

import cz.muni.ics.perun_ssi_issuer.middleware.facade.TranslationFacade;
import cz.muni.ics.perun_ssi_issuer.web.model.references.CredentialDefinitionReference;
import cz.muni.ics.perun_ssi_issuer.web.model.references.SchemaAttributeDefinitionReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class SchemaDefinitionDTO extends DefinitionLocalization {

    @NotNull
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String version;

    @NotNull
    @Builder.Default
    private List<CredentialDefinitionReference> credentialDefinitionReferences = new ArrayList<>();

    @NotNull
    @Builder.Default
    private Set<SchemaAttributeDefinitionReference> attributeDefinitionReferences = new HashSet<>();

    @Override
    public String i18nNameKey() {
        return TranslationFacade.getSchDefI18nNameKey(getName());
    }

    @Override
    public String i18nDescKey() {
        return TranslationFacade.getSchDefI18nDescKey(getName());
    }

}
