package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationExchangeNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedPresentationExchangeAccessException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationExchangeEntity;
import lombok.NonNull;
import org.hyperledger.aries.api.present_proof_v2.V20PresExRecord;

public interface PresentationExchangeFacade {
    PresentationExchangeEntity createPresentationExchange(@NonNull Long schemaId,
                                                          @NonNull Long connectionId,
                                                          @NonNull String userIdentifier)
            throws ConnectionNotFoundException, UnauthorizedConnectionAccessException,
            SchemaDefinitionNotFoundException;

    PresentationExchangeEntity getPresentationExchange(@NonNull Long presExchId,
                                                       @NonNull String userIdentifier)
            throws PresentationExchangeNotFoundException, UnauthorizedPresentationExchangeAccessException,
            DatabaseInconsistencyException;

    void manuallyCheckPresentationExchange(@NonNull Long presExchId, @NonNull String userIdentifier)
            throws UnauthorizedPresentationExchangeAccessException, PresentationExchangeNotFoundException,
            DatabaseInconsistencyException;

    void handleWebhook(@NonNull V20PresExRecord proof);

}
