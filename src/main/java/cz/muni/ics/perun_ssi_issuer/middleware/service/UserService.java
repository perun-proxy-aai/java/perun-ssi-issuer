package cz.muni.ics.perun_ssi_issuer.middleware.service;

import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserIdentifierAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserUpdateException;
import cz.muni.ics.perun_ssi_issuer.web.model.UserDetailsDTO;
import lombok.NonNull;
import org.springframework.security.core.Authentication;

public interface UserService {

    UserDetailsDTO saveOrUpdateUserDetails(@NonNull Authentication auth)
            throws NoUserIdentifierAvailableException, NoUserDetailsAvailableException,
            UserUpdateException;

}
