package cz.muni.ics.perun_ssi_issuer.data.perun.enums;

public enum MemberStatus {

    VALID,
    EXPIRED,
    INVALID,
    DISABLED,
    SUSPENDED

}
