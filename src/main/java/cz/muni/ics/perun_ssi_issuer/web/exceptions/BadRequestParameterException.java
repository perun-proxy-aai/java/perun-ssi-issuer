package cz.muni.ics.perun_ssi_issuer.web.exceptions;

public class BadRequestParameterException extends Exception {

    public BadRequestParameterException() {
        super();
    }

    public BadRequestParameterException(String message) {
        super(message);
    }

    public BadRequestParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestParameterException(Throwable cause) {
        super(cause);
    }

    protected BadRequestParameterException(String message,
                                           Throwable cause,
                                           boolean enableSuppression,
                                           boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
