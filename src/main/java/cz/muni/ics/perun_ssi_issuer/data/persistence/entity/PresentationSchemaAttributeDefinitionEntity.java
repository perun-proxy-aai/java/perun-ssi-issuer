package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import cz.muni.ics.perun_ssi_issuer.common.enums.SchemaAttributeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
//DB
@Entity(name = "PresentationSchemaAttributeDefinition")
@Table(
        name = "pres_schema_attribute_definitions",
        indexes = @Index(name = "idx_pres_sch_attr_defs_name", columnList = "name",
                unique = true),
        uniqueConstraints = @UniqueConstraint(name = "constr_unique_pres_sch_attr_defs_name",
                columnNames = "name")
)
public class PresentationSchemaAttributeDefinitionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "attribute_type", nullable = false)
    private SchemaAttributeType attributeType;

    @NotNull
    @ManyToMany(mappedBy = "attributeDefinitions")
    @ToString.Exclude
    @Builder.Default
    private Set<PresentationSchemaDefinitionEntity> schemaDefinitions = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        PresentationSchemaAttributeDefinitionEntity that = (PresentationSchemaAttributeDefinitionEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public void addSchemaDefinition(@NonNull PresentationSchemaDefinitionEntity definition) {
        schemaDefinitions.add(definition);
        definition.getAttributeDefinitions().add(this);
    }

    public void removeSchemaDefinition(@NonNull PresentationSchemaDefinitionEntity definition) {
        schemaDefinitions.remove(definition);
        definition.getAttributeDefinitions().remove(this);
    }

}

