package cz.muni.ics.perun_ssi_issuer.common.exception;

public class SchemaAttributeDefinitionNotFoundException extends Exception {

    public SchemaAttributeDefinitionNotFoundException() {
        super();
    }

    public SchemaAttributeDefinitionNotFoundException(String message) {
        super(message);
    }

    public SchemaAttributeDefinitionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public SchemaAttributeDefinitionNotFoundException(Throwable cause) {
        super(cause);
    }

    protected SchemaAttributeDefinitionNotFoundException(String message, Throwable cause, boolean enableSuppression,
                                                         boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
