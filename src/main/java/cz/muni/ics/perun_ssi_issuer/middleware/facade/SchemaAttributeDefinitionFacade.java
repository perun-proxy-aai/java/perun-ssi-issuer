package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaAttributeDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.SchemaAttributeDefinitionForm;
import lombok.NonNull;

import java.util.List;

public interface SchemaAttributeDefinitionFacade {

    List<SchemaAttributeDefinitionEntity> getDefinitions();


    SchemaAttributeDefinitionEntity getDefinition(@NonNull Long schAttrDefId)
            throws SchemaAttributeDefinitionNotFoundException;

    SchemaAttributeDefinitionEntity createDefinition(@NonNull SchemaAttributeDefinitionForm form);

    SchemaAttributeDefinitionEntity updateDefinition(
            @NonNull SchemaAttributeDefinitionEntity oldEntity,
            @NonNull SchemaAttributeDefinitionForm form
    );

    void deleteDefinition(@NonNull SchemaAttributeDefinitionEntity definition);

}
