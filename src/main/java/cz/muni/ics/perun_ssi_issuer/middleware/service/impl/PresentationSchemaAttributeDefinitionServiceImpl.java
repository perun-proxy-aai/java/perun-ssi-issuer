package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationSchemaAttributeDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.PresentationSchemaAttributeDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.TranslationFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.Translations;
import cz.muni.ics.perun_ssi_issuer.middleware.service.PresentationSchemaAttributeDefinitionService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.EntityMappingUtils;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaAttributeDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentationSchemaAttributeDefinitionForm;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@Slf4j
public class PresentationSchemaAttributeDefinitionServiceImpl implements PresentationSchemaAttributeDefinitionService {

    private final PresentationSchemaAttributeDefinitionFacade schAttrDefFacade;

    private final TranslationFacade translationFacade;

    @Autowired
    public PresentationSchemaAttributeDefinitionServiceImpl(
            @NonNull PresentationSchemaAttributeDefinitionFacade presentationSchemaAttributeDefinitionFacade,
            @NonNull TranslationFacade translationFacade
    ) {
        this.schAttrDefFacade = presentationSchemaAttributeDefinitionFacade;
        this.translationFacade = translationFacade;
    }

    @Override
    public List<PresentationSchemaAttributeDefinitionDTO> getDefinitions() {
        List<PresentationSchemaAttributeDefinitionEntity> definitions = schAttrDefFacade.getDefinitions();
        return EntityMappingUtils.mapPresentationSchemaAttributeDefinitions(definitions);
    }

    @Override
    public PresentationSchemaAttributeDefinitionDTO getDefinition(@NonNull Long id)
            throws PresentationSchemaAttributeDefinitionNotFoundException
    {
        PresentationSchemaAttributeDefinitionEntity definition = schAttrDefFacade.getDefinition(id);
        return EntityMappingUtils.map(definition);
    }

    @Override
    public PresentationSchemaAttributeDefinitionForm getDefinitionForEdit(@NonNull Long id)
            throws PresentationSchemaAttributeDefinitionNotFoundException
    {
        PresentationSchemaAttributeDefinitionForm form = new PresentationSchemaAttributeDefinitionForm();
        PresentationSchemaAttributeDefinitionEntity definition = schAttrDefFacade.getDefinition(id);
        Translations translations = translationFacade.getPresentationSchemaAttributeDefinitionTranslations(definition.getName());

        form.setName(definition.getName());
        form.setType(definition.getAttributeType());
        form.addNameTranslations(translations.getNameTranslations());
        form.addDescriptionTranslations(translations.getDescTranslations());

        return form;
    }

    @Override
    public PresentationSchemaAttributeDefinitionDTO createDefinition(
            @NonNull PresentationSchemaAttributeDefinitionForm form
    ) {
        PresentationSchemaAttributeDefinitionEntity definition = schAttrDefFacade.createDefinition(form);

        translationFacade.createPresentationSchemaAttributeDefinitionTranslations(
                new Translations(form.getNameTranslations(), form.getDescriptionTranslations()),
                definition.getName()
        );

        return EntityMappingUtils.map(definition);
    }

    @Override
    @Transactional
    public PresentationSchemaAttributeDefinitionDTO updateDefinition(@NonNull Long id,
                                                                     @NonNull PresentationSchemaAttributeDefinitionForm form)
            throws PresentationSchemaAttributeDefinitionNotFoundException {

        PresentationSchemaAttributeDefinitionEntity definition = schAttrDefFacade.getDefinition(id);
        String oldName = definition.getName();
        definition = schAttrDefFacade.updateDefinition(definition, form);

        translationFacade.updatePresentationSchemaAttributeDefinitionTranslations(
                new Translations(form.getNameTranslations(), form.getDescriptionTranslations()),
                definition.getName(),
                oldName
        );

        return EntityMappingUtils.map(definition);
    }

    @Override
    @Transactional
    public boolean deleteDefinition(@NonNull Long id)
            throws PresentationSchemaAttributeDefinitionNotFoundException
    {
        PresentationSchemaAttributeDefinitionEntity definition = schAttrDefFacade.getDefinition(id);
        translationFacade.deletePresentationSchemaAttributeDefinitionTranslations(definition.getName());
        schAttrDefFacade.deleteDefinition(definition);
        return true;
    }

}
