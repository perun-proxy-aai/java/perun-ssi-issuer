package cz.muni.ics.perun_ssi_issuer.data.aries.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import cz.muni.ics.perun_ssi_issuer.common.exception.ConfigurationException;
import cz.muni.ics.perun_ssi_issuer.data.aries.AriesAgentController;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.AriesFailureException;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.CredDefAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.SchemaAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.data.aries.properties.AriesAgentProperties;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaDefinitionEntity;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.hyperledger.acy_py.generated.model.V20CredExRecordDetail;
import org.hyperledger.acy_py.generated.model.V20CredFilterIndy;
import org.hyperledger.aries.AriesClient;
import org.hyperledger.aries.api.connection.ConnectionRecord;
import org.hyperledger.aries.api.connection.CreateInvitationRequest;
import org.hyperledger.aries.api.connection.CreateInvitationResponse;
import org.hyperledger.aries.api.credential_definition.CredentialDefinition.CredentialDefinitionRequest;
import org.hyperledger.aries.api.credential_definition.CredentialDefinitionFilter;
import org.hyperledger.aries.api.credentials.CredentialAttributes;
import org.hyperledger.aries.api.exception.AriesException;
import org.hyperledger.aries.api.issue_credential_v2.V20CredExRecord;
import org.hyperledger.aries.api.issue_credential_v2.V2CredentialExchangeFree;
import org.hyperledger.aries.api.issue_credential_v2.V2CredentialExchangeFree.V20CredFilter;
import org.hyperledger.aries.api.issue_credential_v2.V2CredentialExchangeFree.V2CredentialPreview;
import org.hyperledger.aries.api.present_proof.PresentProofRequest;
import org.hyperledger.aries.api.present_proof.PresentProofRequestHelper;
import org.hyperledger.aries.api.present_proof_v2.V20PresExRecord;
import org.hyperledger.aries.api.present_proof_v2.V20PresSendRequestRequest;
import org.hyperledger.aries.api.schema.SchemaSendRequest;
import org.hyperledger.aries.api.schema.SchemaSendResponse;
import org.hyperledger.aries.api.schema.SchemaSendResponse.Schema;
import org.hyperledger.aries.api.schema.SchemasCreatedFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.hyperledger.aries.api.credential_definition.CredentialDefinition.CredentialDefinitionResponse;
import static org.hyperledger.aries.api.credential_definition.CredentialDefinition.CredentialDefinitionsCreated;
import static org.hyperledger.aries.api.present_proof_v2.V20PresSendRequestRequest.V20PresRequestByFormat;
import static org.hyperledger.aries.api.present_proof_v2.V20PresSendRequestRequest.builder;

@Slf4j
@Component
public class AriesAgentControllerImpl implements AriesAgentController {

    private final AriesClient ariesClient;

    private final AriesAgentProperties agentProperties;

    @Autowired
    public AriesAgentControllerImpl(@NonNull AriesClient ariesClient,
                                    @NonNull AriesAgentProperties agentProperties)
    {
        this.ariesClient = ariesClient;
        this.agentProperties = agentProperties;
    }

    @Override
    public CreateInvitationResponse createInvitation() {
        CreateInvitationRequest request = CreateInvitationRequest.builder()
                .myLabel(agentProperties.getInvitationLabel())
                .build();
        try {
            return ariesClient.connectionsCreateInvitation(request)
                    .orElseThrow(() -> new AriesFailureException("Failed to create invitation"));
        } catch (IOException e) {
            throw new AriesFailureException(e);
        }
    }

    @Override
    public ConnectionRecord getConnection(@NonNull String connectionId) {
        try {
            return ariesClient.connectionsGetById(connectionId)
                    .orElseThrow(() -> new AriesFailureException("Connection not found in agent"));
        } catch (IOException e) {
            throw new AriesFailureException(e);
        }
    }

    @Override
    public V20CredExRecord sendCredential(@NonNull String credentialDefinitionId,
                                          @NonNull String connectionId,
                                          @NonNull String schemaId,
                                          @NonNull String comment,
                                          @NonNull JsonNode userAttributes)
    {
        Schema schema = getSchema(schemaId);
        if (schema == null) {
            throw new RuntimeException();
        }
        List<CredentialAttributes> attributes = serializePerunAttributes(userAttributes);
        V20CredFilter filter = V20CredFilter.builder()
                .indy(V20CredFilterIndy.builder().credDefId(credentialDefinitionId).build())
                .build();
        V2CredentialPreview preview = V2CredentialPreview.builder()
                .attributes(attributes)
                .build();
        V2CredentialExchangeFree request = V2CredentialExchangeFree.builder()
                .comment(comment)
                .autoIssue(true)
                .connectionId(UUID.fromString(connectionId))
                .filter(filter)
                .credentialPreview(preview)
                .build();
        try {
            return ariesClient.issueCredentialV2Send(request)
                    .orElseThrow(() -> new AriesFailureException("Failed to issue credential in Aries call"));
        } catch (IOException e) {
            throw new AriesFailureException(e);
        }
    }

    @Override
    public V20CredExRecordDetail getCredentialExchange(String credentialExchangeId) {
        try {
            return ariesClient.issueCredentialV2RecordsGetById(credentialExchangeId)
                    .orElseThrow(() -> new AriesFailureException("Failed to get credential exchange by ID in Agent"));
        } catch (IOException e) {
            throw new AriesFailureException(e);
        }
    }

    @Override
    public String createCredentialDefinition(@NonNull SchemaDefinitionEntity schemaDefinition,
                                             boolean supportRevocation,
                                             Integer revocationRegistrySize)
            throws CredDefAlreadyExistsException
    {
        CredentialDefinitionRequest.CredentialDefinitionRequestBuilder requestBuilder
                = CredentialDefinitionRequest.builder()
                .schemaId(schemaDefinition.getSchemaDefinitionId())
                .tag(schemaDefinition.getName() + '_' + schemaDefinition.getVersion());

        requestBuilder = requestBuilder.supportRevocation(supportRevocation);
        if (supportRevocation) {
            if (revocationRegistrySize == null) {
                throw new ConfigurationException(
                        "At this stage, revocation registry size should be decided");
            } else {
                requestBuilder = requestBuilder.revocationRegistrySize(revocationRegistrySize);
            }
        }
        try {
            CredentialDefinitionResponse response = ariesClient.credentialDefinitionsCreate(requestBuilder.build())
                    .orElse(null);
            if (response == null || !StringUtils.hasText(response.getCredentialDefinitionId())) {
                throw new AriesFailureException("Creating credential definition has not completed correctly");
            }
            return response.getCredentialDefinitionId();
        } catch (IOException e) {
            throw new AriesFailureException(e);
        } catch (AriesException e) {
            if (e.getCode() == 400) {
                List<String> definitions = getCredentialDefinitions(
                        null, schemaDefinition.getSchemaDefinitionId(), null, null, null, null);
                if (definitions != null && definitions.size() == 1) {
                    throw new CredDefAlreadyExistsException(definitions.get(0));
                }
            }
            throw new AriesFailureException(e);
        }
    }

    @Override
    public String createSchema(@NonNull SchemaDefinitionEntity definition) throws SchemaAlreadyExistsException {
        List<String> attributeNames = definition.getAttributeDefinitions()
                .stream()
                .map(SchemaAttributeDefinitionEntity::getName)
                .collect(Collectors.toList());
        SchemaSendRequest request = SchemaSendRequest.builder()
                .schemaName(definition.getName())
                .schemaVersion(definition.getVersion())
                .attributes(attributeNames)
                .build();
        try {
            SchemaSendResponse response = ariesClient.schemas(request).orElse(null);
            if (response == null || !StringUtils.hasText(response.getSchemaId())) {
                throw new AriesFailureException("Creating schema definition has not completed correctly");
            }
            return response.getSchemaId();
        } catch (IOException e) {
            throw new AriesFailureException(e);
        } catch (AriesException e) {
            if (e.getCode() == 400) {
                List<String> schemas = getSchemas(null, null, definition.getName(), definition.getVersion());
                if (schemas != null && schemas.size() == 1) {
                    throw new SchemaAlreadyExistsException(schemas.get(0));
                }
            }
            throw new AriesFailureException(e);
        }
    }

    @Override
    public V20PresExRecord createPresentationExchange(@NonNull PresentationSchemaDefinitionEntity schema,
                                                      @NonNull ConnectionEntity connection)
    {
        Set<String> attrNames = schema.getAttributeDefinitions().stream()
                .map(PresentationSchemaAttributeDefinitionEntity::getName)
                .collect(Collectors.toSet());
        PresentProofRequest.ProofRequest.ProofRestrictions restrictions =
                PresentProofRequest.ProofRequest.ProofRestrictions.builder()
                        .schemaId(schema.getSchemaDefinitionId())
                        .build();
        PresentProofRequest presentProofRequest = PresentProofRequestHelper.buildForAllAttributes(
                connection.getConnectionId(),
                attrNames,
                List.of(restrictions)
        );
        V20PresRequestByFormat presentationRequest = V20PresRequestByFormat.builder()
                        .indy(presentProofRequest.getProofRequest())
                        .build();
        V20PresSendRequestRequest request = builder()
                .presentationRequest(presentationRequest)
                .autoVerify(true)
                .trace(true)
                .connectionId(connection.getConnectionId())
                .build();
        try {
            return ariesClient.presentProofV2SendRequest(request)
                    .orElseThrow(() -> new AriesFailureException("Failed to send proof request"));
        } catch (IOException e) {
            throw new AriesFailureException(e);
        }
    }

    @Override
    public V20PresExRecord getPresentationExchange(@NonNull String presExchId) {
        try {
            return ariesClient.presentProofV2RecordsGetById(presExchId)
                    .orElseThrow(() -> new AriesFailureException("Failed to get PresentationExchangeRecord"));
        } catch (IOException e) {
            throw new AriesFailureException(e);
        }
    }

    // SO FAR ONLY LOCAL METHODS, useful for admins

    private List<String> getSchemas(String schemaId,
                                   String schemaIssuerDid,
                                   String schemaName,
                                   String schemaVersion) {
        SchemasCreatedFilter filter = SchemasCreatedFilter.builder()
                .schemaId(schemaId)
                .schemaIssuerDid(schemaIssuerDid)
                .schemaName(schemaName)
                .schemaVersion(schemaVersion)
                .build();
        try {
            return ariesClient.schemasCreated(filter)
                    .orElseThrow(() -> new AriesFailureException("Failed to get created schemas"));
        } catch (IOException e) {
            throw new AriesFailureException(e);
        }
    }

    private Schema getSchema(@NonNull String schemaId) {
        try {
            return ariesClient.schemasGetById(schemaId)
                    .orElseThrow(() -> new AriesFailureException("Failed to get schema"));
        } catch (IOException e) {
            throw new AriesFailureException(e);
        }
    }

    private List<String> getCredentialDefinitions(String credDefId,
                                                 String schemaId,
                                                 String schemaVersion,
                                                 String schemaName,
                                                 String schemaIssuerDid,
                                                 String issuerDid
    ) {
        CredentialDefinitionFilter filter = CredentialDefinitionFilter.builder()
                .credDefId(credDefId)
                .schemaId(schemaId)
                .schemaName(schemaName)
                .schemaVersion(schemaVersion)
                .schemaIssuerDid(schemaIssuerDid)
                .issuerDid(issuerDid)
                .build();
        try {
            CredentialDefinitionsCreated response = ariesClient.credentialDefinitionsCreated(filter)
                    .orElseThrow(() -> new AriesFailureException("Failed to get created credential definitions"));
            return response.getCredentialDefinitionIds();
        } catch (IOException e) {
            throw new AriesFailureException(e);
        }
    }

    private List<CredentialAttributes> serializePerunAttributes(@NotNull JsonNode userAttributes) {
        List<CredentialAttributes> list = new ArrayList<>();
        Iterator<String> names = userAttributes.fieldNames();
        while (names.hasNext()) {
            String name = names.next();
            JsonNode jsonValue = userAttributes.get(name);
            String strValue = jsonValue.isValueNode() ? jsonValue.asText() : null;
            if (strValue == null) {
                if (jsonValue instanceof ObjectNode) {
                    strValue = jsonValue.toString();
                } else if (jsonValue instanceof ArrayNode) {
                    StringJoiner joiner = new StringJoiner(";");
                    for (JsonNode item : jsonValue) {
                        joiner.add(item.asText());
                    }
                    strValue = joiner.toString();
                }
            }
            list.add(CredentialAttributes.builder()
                    .name(name)
                    .value(strValue)
                    //.mimeType() //TODO
                    .build());
        }
        return list;
    }

}
