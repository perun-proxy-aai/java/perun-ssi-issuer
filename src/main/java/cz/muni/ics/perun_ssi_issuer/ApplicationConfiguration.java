package cz.muni.ics.perun_ssi_issuer;

import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationPropertiesScan(basePackages = "cz.muni.ics")
public class ApplicationConfiguration {

}
