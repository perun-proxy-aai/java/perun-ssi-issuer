package cz.muni.ics.perun_ssi_issuer.data.perun.adapter;


import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.perun_ssi_issuer.data.perun.connector.PerunConnector;
import cz.muni.ics.perun_ssi_issuer.data.perun.exceptions.PerunUserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.Attribute;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.User;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
@Component
public class PerunAdapterRpc implements PerunAdapter {

    private static final String EXT_SOURCE_TYPE_IDP = "cz.metacentrum.perun.core.impl.ExtSourceIdp";

    @NonNull
    private final PerunConnector rpcConnector;

    @NonNull
    private final PerunAdapterProperties adapterProperties;

    @Autowired
    public PerunAdapterRpc(@NonNull PerunConnector rpcConnector,
                           @NonNull PerunAdapterProperties adapterProperties) {
        this.rpcConnector = rpcConnector;
        this.adapterProperties = adapterProperties;
    }

    @Override
    public User identifyPerunUser(@NonNull String userIdentifier) throws PerunUserNotFoundException {
        if (!StringUtils.hasText(userIdentifier)) {
            throw new IllegalArgumentException("UserIdentifier cannot be empty");
        }

        User user = rpcConnector.getUserByExtSourceNameAndExtLogin(
                adapterProperties.getExtSourceName(), userIdentifier);
        if (user == null) {
            log.debug("User with identifier '{}' not found, or found multiple users with the " +
                    "same identifier", userIdentifier);
            throw new PerunUserNotFoundException("Could not identify user by given identifier");
        }
        return user;
    }

    @Override
    public Map<String, JsonNode> getUserAttributes(@NonNull User user,
                                                   @NonNull Set<String> attributesToFetch) {
        if (attributesToFetch.isEmpty()) {
            throw new IllegalArgumentException("Provided empty map of attributes for fetching");
        }
        List<Attribute> attributes = rpcConnector.getAttributes(user, attributesToFetch);
        Map<String, JsonNode> result = new HashMap<>();
        for (Attribute a : attributes) {
            result.put(a.getFullName(), a.getValue());
        }
        return result;
    }

}
