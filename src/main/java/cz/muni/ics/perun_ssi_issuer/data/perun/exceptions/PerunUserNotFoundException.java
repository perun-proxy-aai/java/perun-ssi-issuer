package cz.muni.ics.perun_ssi_issuer.data.perun.exceptions;

public class PerunUserNotFoundException extends Exception {

    public PerunUserNotFoundException() {
        super();
    }

    public PerunUserNotFoundException(String s) {
        super(s);
    }

    public PerunUserNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public PerunUserNotFoundException(Throwable throwable) {
        super(throwable);
    }

    protected PerunUserNotFoundException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
