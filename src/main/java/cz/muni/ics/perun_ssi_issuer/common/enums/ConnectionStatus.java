package cz.muni.ics.perun_ssi_issuer.common.enums;

import org.hyperledger.aries.api.connection.ConnectionState;

public enum ConnectionStatus {

    INIT,
    START,
    ACTIVE,
    ERROR,
    REQUEST,
    RESPONSE,
    ABANDONED,
    COMPLETED,
    INVITATION,
    PING_RESPONSE,
    PING_NO_RESPONSE;

    public static ConnectionStatus fromState(ConnectionState state) {
        switch (state) {
            case INIT:
                return INIT;
            case START:
                return START;
            case ACTIVE:
                return ACTIVE;
            case ERROR:
                return ERROR;
            case REQUEST:
                return REQUEST;
            case RESPONSE:
                return RESPONSE;
            case ABANDONED:
                return ABANDONED;
            case COMPLETED:
                return COMPLETED;
            case INVITATION:
                return INVITATION;
            case PING_RESPONSE:
                return PING_RESPONSE;
            case PING_NO_RESPONSE:
                return PING_NO_RESPONSE;
            default:
                return null;
        }
    }
}
