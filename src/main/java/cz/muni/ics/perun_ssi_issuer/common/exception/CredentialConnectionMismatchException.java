package cz.muni.ics.perun_ssi_issuer.common.exception;

public class CredentialConnectionMismatchException extends Exception {

    public CredentialConnectionMismatchException() {
        super();
    }

    public CredentialConnectionMismatchException(String message) {
        super(message);
    }

    public CredentialConnectionMismatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public CredentialConnectionMismatchException(Throwable cause) {
        super(cause);
    }

    protected CredentialConnectionMismatchException(String message, Throwable cause, boolean enableSuppression,
                                                    boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
