package cz.muni.ics.perun_ssi_issuer.common.exception;

public class PresentationSchemaAttributeDefinitionNotFoundException extends Exception {

    public PresentationSchemaAttributeDefinitionNotFoundException() {
        super();
    }

    public PresentationSchemaAttributeDefinitionNotFoundException(String message) {
        super(message);
    }

    public PresentationSchemaAttributeDefinitionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PresentationSchemaAttributeDefinitionNotFoundException(Throwable cause) {
        super(cause);
    }

    protected PresentationSchemaAttributeDefinitionNotFoundException(String message, Throwable cause, boolean enableSuppression,
                                                                     boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
