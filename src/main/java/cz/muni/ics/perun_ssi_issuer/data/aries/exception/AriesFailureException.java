package cz.muni.ics.perun_ssi_issuer.data.aries.exception;

public class AriesFailureException extends RuntimeException {

    public AriesFailureException() {
        super();
    }

    public AriesFailureException(String message) {
        super(message);
    }

    public AriesFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    public AriesFailureException(Throwable cause) {
        super(cause);
    }

    protected AriesFailureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
