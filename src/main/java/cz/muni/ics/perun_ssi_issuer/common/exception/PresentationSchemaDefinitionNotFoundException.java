package cz.muni.ics.perun_ssi_issuer.common.exception;

public class PresentationSchemaDefinitionNotFoundException extends Exception {

    public PresentationSchemaDefinitionNotFoundException() {
        super();
    }

    public PresentationSchemaDefinitionNotFoundException(String message) {
        super(message);
    }

    public PresentationSchemaDefinitionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PresentationSchemaDefinitionNotFoundException(Throwable cause) {
        super(cause);
    }

    protected PresentationSchemaDefinitionNotFoundException(String message, Throwable cause, boolean enableSuppression,
                                                            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
