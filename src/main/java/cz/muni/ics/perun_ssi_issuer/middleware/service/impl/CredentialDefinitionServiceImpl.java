package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.CredDefAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.CredentialDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.service.CredentialDefinitionService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.EntityMappingUtils;
import cz.muni.ics.perun_ssi_issuer.web.model.CredentialDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.CredentialDefinitionForm;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("credentialDefinitionService")
public class CredentialDefinitionServiceImpl implements CredentialDefinitionService {

    private final CredentialDefinitionFacade definitionFacade;

    @Autowired
    public CredentialDefinitionServiceImpl(@NonNull CredentialDefinitionFacade definitionFacade) {
        this.definitionFacade = definitionFacade;
    }

    @Override
    public List<CredentialDefinitionDTO> getDefinitions() {
        List<CredentialDefinitionEntity> definitions = definitionFacade.getDefinitions();
        return EntityMappingUtils.mapCredentialDefinitions(definitions);
    }

    @Override
    public CredentialDefinitionDTO getDefinition(@NonNull Long id) throws CredentialDefinitionNotFoundException {
        CredentialDefinitionEntity definition = definitionFacade.getCredentialDefinition(id);
        return EntityMappingUtils.map(definition);
    }

    @Transactional
    @Override
    public CredentialDefinitionDTO createDefinition(@NonNull CredentialDefinitionForm form)
            throws SchemaDefinitionNotFoundException, CredDefAlreadyExistsException
    {
        CredentialDefinitionEntity entity = definitionFacade.createCredentialDefinition(
                form.getSchemaDefinition(),
                form.isSupportRevocation(),
                form.getRevocationRegistrySize()
        );
        return EntityMappingUtils.map(entity);
    }

    @Transactional
    @Override
    public boolean deleteDefinition(@NonNull Long id) throws CredentialDefinitionNotFoundException {
        definitionFacade.deleteCredentialDefinition(id);
        return true;
    }

}
