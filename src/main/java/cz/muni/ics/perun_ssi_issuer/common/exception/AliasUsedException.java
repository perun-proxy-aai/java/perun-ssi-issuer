package cz.muni.ics.perun_ssi_issuer.common.exception;

public class AliasUsedException extends RuntimeException {

    public AliasUsedException() {
        super();
    }

    public AliasUsedException(String message) {
        super(message);
    }

    public AliasUsedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AliasUsedException(Throwable cause) {
        super(cause);
    }

    protected AliasUsedException(String message,
                                 Throwable cause,
                                 boolean enableSuppression,
                                 boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
