package cz.muni.ics.perun_ssi_issuer.common.exception;

public class UnauthorizedInvitationAccessException extends Exception {

    public UnauthorizedInvitationAccessException() {
        super();
    }

    public UnauthorizedInvitationAccessException(String message) {
        super(message);
    }

    public UnauthorizedInvitationAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedInvitationAccessException(Throwable cause) {
        super(cause);
    }

    protected UnauthorizedInvitationAccessException(String message, Throwable cause,
                                                    boolean enableSuppression,
                                                    boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
