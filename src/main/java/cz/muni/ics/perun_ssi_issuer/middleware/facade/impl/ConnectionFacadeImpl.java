package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import cz.muni.ics.perun_ssi_issuer.common.enums.ConnectionStatus;
import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.UserEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.ConnectionRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.CredentialRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.UserRepository;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.ConnectionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.ConnectionCredentials;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ConnectionFacadeImpl implements ConnectionFacade {
    private final UserRepository userRepository;

    private final ConnectionRepository connectionRepository;

    private final CredentialRepository credentialRepository;

    @Autowired
    public ConnectionFacadeImpl(ConnectionRepository connectionRepository,
                                CredentialRepository credentialRepository,
                                UserRepository userRepository) {
        this.connectionRepository = connectionRepository;
        this.credentialRepository = credentialRepository;
        this.userRepository = userRepository;
    }

    @Override
    public ConnectionEntity getConnection(@NonNull Long id, @NonNull String userIdentifier)
            throws ConnectionNotFoundException, DatabaseInconsistencyException,
            UnauthorizedConnectionAccessException
    {
        ConnectionEntity connection = connectionRepository.findById(id).orElse(null);
        if (connection == null) {
            log.warn("Connection with ID '{}' not found", id);
            throw new ConnectionNotFoundException();
        }

        if (connection.getUser() == null) {
            log.error("DB ERROR - Connection '{}' has no user object assigned", connection);
            throw new DatabaseInconsistencyException();
        } else if (!userIdentifier.equals(connection.getUser().getIdentifier())) {
            log.error("User with identifier '{}' tried to load foreign connection '{}'",
                    userIdentifier, connection);
            throw new UnauthorizedConnectionAccessException();
        }
        return connection;
    }

    @Override
    public ConnectionCredentials getConnectionCredentials(@NonNull ConnectionEntity connection) {
        List<CredentialEntity> activeCredentials = credentialRepository
                .findAllByConnectionAndActiveAndProcessed(connection, true, true);
        List<CredentialEntity> revokedCredentials = credentialRepository
                .findAllByConnectionAndActiveAndProcessed(connection, false, true);
        List<CredentialEntity> inProcessing = credentialRepository
                .findAllByConnectionAndProcessed(connection, false);

        return new ConnectionCredentials(activeCredentials, revokedCredentials, inProcessing);
    }

    @Override
    public List<ConnectionEntity> getActiveUserConnections(@NonNull String userIdentifier) throws UserNotFoundException {
        UserEntity userEntity = userRepository.findByIdentifier(userIdentifier).orElse(null);
        if (userEntity == null) {
            log.warn("User with identifier '{}' not found", userIdentifier);
            throw new UserNotFoundException();
        }
        return connectionRepository.findAllByUserAndStatus(userEntity, ConnectionStatus.ACTIVE);
    }

}
