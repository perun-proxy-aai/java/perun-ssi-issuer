package cz.muni.ics.perun_ssi_issuer.middleware.service.util;

import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

@Slf4j
public class ServiceUtils {

    public static String getUserIdentifier(@NonNull Authentication auth) throws
            NoUserDetailsAvailableException {
        if (auth.getPrincipal() instanceof OidcUser) {
            OidcUser user = (OidcUser) auth.getPrincipal();
            if (user != null) {
                return user.getSubject();
            }
            log.error("No user details available in auth '{}'", auth);
            throw new NoUserDetailsAvailableException();
        }
        log.error("Logged in user is not instance of OidcUser (principal: '{}')",
                auth.getPrincipal());
        throw new NoUserDetailsAvailableException();
    }

}
