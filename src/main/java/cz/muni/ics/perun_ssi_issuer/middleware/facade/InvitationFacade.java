package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.InvitationNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedInvitationAccessException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.InvitationEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.UserEntity;
import lombok.NonNull;
import org.hyperledger.aries.api.connection.ConnectionRecord;

import java.util.List;

public interface InvitationFacade {

    InvitationEntity createInvitation(@NonNull String userAlias, @NonNull UserEntity user);

    InvitationEntity getInvitation(@NonNull Long id, @NonNull String userIdentifier)
            throws InvitationNotFoundException, UnauthorizedInvitationAccessException;

    List<InvitationEntity> getPendingInvitations(@NonNull String userIdentifier);

    ConnectionEntity getConnectionByInvitation(@NonNull Long id, @NonNull String userIdentifier)
            throws ConnectionNotFoundException, InvitationNotFoundException,
            UnauthorizedConnectionAccessException, UnauthorizedInvitationAccessException;

    void handleWebhook(@NonNull ConnectionRecord connectionRecord);

    void cancelInvitation(@NonNull Long id, @NonNull String userIdentifier)
            throws InvitationNotFoundException, UnauthorizedInvitationAccessException;

    void manuallyCheckInvitation(@NonNull Long id, @NonNull String userIdentifier)
            throws InvitationNotFoundException, UnauthorizedInvitationAccessException;

}
