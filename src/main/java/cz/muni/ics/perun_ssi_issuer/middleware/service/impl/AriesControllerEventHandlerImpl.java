package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.middleware.facade.CredentialFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.InvitationFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.PresentationExchangeFacade;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.hyperledger.acy_py.generated.model.IssuerRevRegRecord;
import org.hyperledger.aries.api.connection.ConnectionRecord;
import org.hyperledger.aries.api.issue_credential_v1.V1CredentialExchange;
import org.hyperledger.aries.api.issue_credential_v2.V20CredExRecord;
import org.hyperledger.aries.api.issue_credential_v2.V2IssueIndyCredentialEvent;
import org.hyperledger.aries.api.issue_credential_v2.V2IssueLDCredentialEvent;
import org.hyperledger.aries.api.message.ProblemReport;
import org.hyperledger.aries.api.present_proof_v2.V20PresExRecord;
import org.hyperledger.aries.api.revocation.RevocationEvent;
import org.hyperledger.aries.api.revocation.RevocationNotificationEvent;
import org.hyperledger.aries.api.revocation.RevocationNotificationEventV2;
import org.hyperledger.aries.api.settings.Settings;
import org.hyperledger.aries.webhook.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AriesControllerEventHandlerImpl extends EventHandler {

    private final InvitationFacade invitationFacade;

    private final CredentialFacade credentialFacade;

    private final PresentationExchangeFacade presentationExchangeFacade;

    @Autowired
    public AriesControllerEventHandlerImpl(@NonNull InvitationFacade invitationFacade,
                                           @NonNull CredentialFacade credentialFacade,
                                           @NonNull PresentationExchangeFacade presentationExchangeFacade)
    {
        this.invitationFacade = invitationFacade;
        this.credentialFacade = credentialFacade;
        this.presentationExchangeFacade = presentationExchangeFacade;
    }

    @Override
    public void handleCredential(V1CredentialExchange credential) {
        super.handleCredential(credential);
    }

    @Override
    public void handleCredentialV2(V20CredExRecord v20Credential) {
        credentialFacade.handleWebhookCall(v20Credential);
    }

    @Override
    public void handleIssueCredentialV2Indy(V2IssueIndyCredentialEvent credentialInfo) {
        super.handleIssueCredentialV2Indy(credentialInfo);
    }

    @Override
    public void handleIssueCredentialV2LD(V2IssueLDCredentialEvent credentialInfo) {
        super.handleIssueCredentialV2LD(credentialInfo);
    }

    @Override
    public void handleRevocation(RevocationEvent revocation) {
        super.handleRevocation(revocation);
    }

    @Override
    public void handleRevocationNotification(RevocationNotificationEvent revocationNotification) {
        super.handleRevocationNotification(revocationNotification);
    }

    @Override
    public void handleRevocationNotificationV2(
            RevocationNotificationEventV2 revocationNotificationV2) {
        super.handleRevocationNotificationV2(revocationNotificationV2);
    }

    @Override
    public void handleRevocationRegistry(IssuerRevRegRecord revocationRegistry) {
        super.handleRevocationRegistry(revocationRegistry);
    }

    @Override
    public void handleConnection(ConnectionRecord connection) {
        invitationFacade.handleWebhook(connection);
    }

    @Override
    public void handleProblemReport(ProblemReport report) {
        super.handleProblemReport(report);
    }

    @Override
    public void handleSettings(Settings settings) {
        super.handleSettings(settings);
    }

    @Override
    public void handleProofV2(V20PresExRecord proof) {
        presentationExchangeFacade.handleWebhook(proof);
    }

}
