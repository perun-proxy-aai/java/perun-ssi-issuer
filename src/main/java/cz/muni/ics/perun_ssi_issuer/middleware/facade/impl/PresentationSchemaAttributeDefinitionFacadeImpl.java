package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationSchemaAttributeDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.PresentationSchemaAttributeDefinitionRepository;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.PresentationSchemaAttributeDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentationSchemaAttributeDefinitionForm;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

@Service
public class PresentationSchemaAttributeDefinitionFacadeImpl implements PresentationSchemaAttributeDefinitionFacade {

    private final PresentationSchemaAttributeDefinitionRepository repository;

    @Autowired
    public PresentationSchemaAttributeDefinitionFacadeImpl(
            @NonNull PresentationSchemaAttributeDefinitionRepository repository
    ) {
        this.repository = repository;
    }

    @Override
    public List<PresentationSchemaAttributeDefinitionEntity> getDefinitions() {
        return repository.findAll();
    }

    @Override
    public PresentationSchemaAttributeDefinitionEntity getDefinition(@NonNull Long id)
            throws PresentationSchemaAttributeDefinitionNotFoundException
    {
        return repository.findById(id).orElseThrow(PresentationSchemaAttributeDefinitionNotFoundException::new);
    }

    @Override
    public PresentationSchemaAttributeDefinitionEntity createDefinition(
            @NonNull PresentationSchemaAttributeDefinitionForm form
    ) {
        PresentationSchemaAttributeDefinitionEntity definition = PresentationSchemaAttributeDefinitionEntity.builder()
                .name(form.getName())
                .schemaDefinitions(new HashSet<>())
                .attributeType(form.getType())
                .build();
        return repository.save(definition);
    }

    @Override
    public PresentationSchemaAttributeDefinitionEntity updateDefinition(
            @NonNull PresentationSchemaAttributeDefinitionEntity oldEntity,
            @NonNull PresentationSchemaAttributeDefinitionForm form
    ) {
        oldEntity.setName(form.getName());
        return repository.save(oldEntity);
    }

    @Override
    public void deleteDefinition(@NonNull PresentationSchemaAttributeDefinitionEntity definition) {
        repository.delete(definition);
    }

}
