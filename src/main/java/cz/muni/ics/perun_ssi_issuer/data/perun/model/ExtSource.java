package cz.muni.ics.perun_ssi_issuer.data.perun.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Validated
public class ExtSource {

    public static final String BEAN_NAME = "ExtSource";

    @NotNull
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String type;

}
