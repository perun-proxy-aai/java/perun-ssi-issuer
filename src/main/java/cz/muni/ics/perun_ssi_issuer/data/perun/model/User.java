package cz.muni.ics.perun_ssi_issuer.data.perun.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.StringUtils;

/**
 * Representation of Perun User.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>;
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class User {

    @NonNull
    private Long id;
    private String firstName;
    private String middleName;
    @NonNull
    private String lastName;
    private String titleBefore;
    private String titleAfter;

    public User(@NonNull Long id,
                String firstName,
                String middleName,
                @NonNull String lastName,
                String titleBefore,
                String titleAfter) {
        this.setId(id);
        this.setFirstName(firstName);
        this.setMiddleName(middleName);
        this.setLastName(lastName);
        this.setTitleBefore(titleBefore);
        this.setTitleAfter(titleAfter);
    }

    public void setLastName(@NonNull String lastName) {
        if (!StringUtils.hasText(lastName)) {
            throw new IllegalArgumentException("LastName cannot be empty");
        }
        this.lastName = lastName;
    }
}
