package cz.muni.ics.perun_ssi_issuer.middleware.service.properties;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class CredentialDefinition implements InitializingBean {

    @NotBlank
    private String schemaName;

    @NotBlank
    private String schemaVersion;

    private String tag;

    private Boolean supportRevocation = true;

    private Integer revocationRegistrySize = 1000;

    @Override
    public void afterPropertiesSet() {
        if (supportRevocation != null && supportRevocation)
            if (revocationRegistrySize != null && revocationRegistrySize < 1) {
                throw new RuntimeException("Invalid configuration - revocation should be supported," +
                        " but revocation registry size is misconfigured '" + revocationRegistrySize + '\'');
            }
    }

}
