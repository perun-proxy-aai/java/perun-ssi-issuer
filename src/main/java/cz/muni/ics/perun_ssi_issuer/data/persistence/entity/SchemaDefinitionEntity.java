package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
//DB
@Entity(name = "SchemaDefinition")
@Table(
        name = "schema_definitions",
        indexes = {
                @Index(name = "idx_schema_defs_schema_definition_id", columnList = "schema_definition_id",
                        unique = true),
                @Index(name = "idx_schema_defs_name_version", columnList = "name,version",
                        unique = true),
        },
        uniqueConstraints = {
                @UniqueConstraint(name = "constr_unique_schema_defs_schema_definition_id",
                        columnNames = "schema_definition_id"),
                @UniqueConstraint(name = "constr_unique_schema_defs_name_version",
                        columnNames = {"name", "version"})
        }
)
public class SchemaDefinitionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "schema_definition_id", nullable = false)
    private String schemaDefinitionId;


    @NotBlank
    @Column(name = "name", nullable = false)
    private String name;

    @NotBlank
    @Pattern(regexp = "[0-9]+\\.[0-9]+")
    @Column(name = "version", nullable = false)
    private String version;

    @NotNull
    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            mappedBy = "schemaDefinition")
    @ToString.Exclude
    @Builder.Default
    private Set<CredentialDefinitionEntity> credentialDefinitions = new HashSet<>();

    @NotEmpty
    @ManyToMany
    @JoinTable(
            name = "schema_definition_schema_attribute_definition",
            joinColumns = @JoinColumn(name = "schema_definition_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "schema_attribute_definition_id", nullable = false)
    )
    @ToString.Exclude
    @Builder.Default
    private Set<SchemaAttributeDefinitionEntity> attributeDefinitions = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        SchemaDefinitionEntity that = (SchemaDefinitionEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public void addCredentialDefinition(@NonNull CredentialDefinitionEntity definition) {
        credentialDefinitions.add(definition);
        definition.setSchemaDefinition(this);
    }

    public void removeCredentialDefinition(@NonNull CredentialDefinitionEntity definition) {
        credentialDefinitions.remove(definition);
        definition.setSchemaDefinition(null);
    }

    public void addSchemaAttributeDefinition(@NonNull SchemaAttributeDefinitionEntity definition) {
        attributeDefinitions.add(definition);
        definition.addSchemaDefinition(this);
    }

    public void removeSchemaAttributeDefinition(@NonNull SchemaAttributeDefinitionEntity definition) {
        attributeDefinitions.remove(definition);
        definition.removeSchemaDefinition(this);
    }

    public void addSchemaAttributeDefinitions(@NonNull Collection<SchemaAttributeDefinitionEntity> attributes) {
        for (SchemaAttributeDefinitionEntity def : attributes) {
            addSchemaAttributeDefinition(def);
        }
    }

    public void removeSchemaAttributeDefinitions(@NonNull Collection<SchemaAttributeDefinitionEntity> attributes) {
        for (SchemaAttributeDefinitionEntity def : attributes) {
            removeSchemaAttributeDefinition(def);
        }
    }

}

