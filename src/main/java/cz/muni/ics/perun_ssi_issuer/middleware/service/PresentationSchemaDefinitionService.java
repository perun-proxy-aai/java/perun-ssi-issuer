package cz.muni.ics.perun_ssi_issuer.middleware.service;

import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationSchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentationSchemaDefinitionForm;
import lombok.NonNull;

import javax.transaction.Transactional;
import java.util.List;

public interface PresentationSchemaDefinitionService {

    List<PresentationSchemaDefinitionDTO> getDefinitions();

    PresentationSchemaDefinitionDTO getDefinition(@NonNull Long schemaDefId)
            throws PresentationSchemaDefinitionNotFoundException;

    PresentationSchemaDefinitionForm getDefinitionForEdit(@NonNull Long id)
            throws PresentationSchemaDefinitionNotFoundException;

    @Transactional
    PresentationSchemaDefinitionDTO createDefinition(@NonNull PresentationSchemaDefinitionForm form);

    @Transactional
    PresentationSchemaDefinitionDTO updateDefinition(@NonNull Long id, @NonNull PresentationSchemaDefinitionForm form)
            throws PresentationSchemaDefinitionNotFoundException;

    @Transactional
    boolean deleteDefinition(@NonNull Long schemaDefId) throws PresentationSchemaDefinitionNotFoundException;

}
