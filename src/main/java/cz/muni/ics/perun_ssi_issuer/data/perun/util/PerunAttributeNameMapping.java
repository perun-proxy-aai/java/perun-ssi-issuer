package cz.muni.ics.perun_ssi_issuer.data.perun.util;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Validated
public class PerunAttributeNameMapping {

    @NotBlank
    private String internalName;

    @NotBlank
    private String rpcName;

}
