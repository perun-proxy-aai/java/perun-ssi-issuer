package cz.muni.ics.perun_ssi_issuer.web.model.forms;

import cz.muni.ics.perun_ssi_issuer.common.enums.SchemaAttributeType;
import cz.muni.ics.perun_ssi_issuer.web.validation.LocalizationMapConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class PresentationSchemaAttributeDefinitionForm {

    @LocalizationMapConstraint
    private final Map<String, String> nameTranslations = new HashMap<>();

    @LocalizationMapConstraint
    private final Map<String, String> descriptionTranslations = new HashMap<>();

    @NotBlank
    private String name;

    @NotNull
    private SchemaAttributeType type;

    public void addNameTranslation(@NonNull String key, @NonNull String value) {
        nameTranslations.put(key, value);
    }

    public void addNameTranslations(@NonNull Map<String, String> entries) {
        nameTranslations.putAll(entries);
    }

    public void removeNameTranslation(@NonNull String key) {
        nameTranslations.remove(key);
    }

    public void removeNameTranslations(@NonNull Collection<String> keys) {
        for (String key : keys) {
            if (key != null) {
                nameTranslations.remove(key);
            }
        }
    }

    public void addDescriptionTranslation(@NonNull String key, @NonNull String value) {
        descriptionTranslations.put(key, value);
    }

    public void addDescriptionTranslations(@NonNull Map<String, String> entries) {
        descriptionTranslations.putAll(entries);
    }

    public void removeDescriptionTranslation(@NonNull String key) {
        descriptionTranslations.remove(key);
    }

    public void removeDescriptionTranslations(@NonNull Collection<String> keys) {
        for (String key : keys) {
            if (key != null) {
                descriptionTranslations.remove(key);
            }
        }
    }

}
