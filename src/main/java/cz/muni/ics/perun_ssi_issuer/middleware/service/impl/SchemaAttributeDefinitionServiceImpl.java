package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaAttributeDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.SchemaAttributeDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.TranslationFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.Translations;
import cz.muni.ics.perun_ssi_issuer.middleware.service.SchemaAttributeDefinitionService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.EntityMappingUtils;
import cz.muni.ics.perun_ssi_issuer.web.model.SchemaAttributeDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.SchemaAttributeDefinitionForm;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@Slf4j
public class SchemaAttributeDefinitionServiceImpl implements SchemaAttributeDefinitionService {

    private final SchemaAttributeDefinitionFacade schAttrDefFacade;

    private final TranslationFacade translationFacade;

    @Autowired
    public SchemaAttributeDefinitionServiceImpl(
            @NonNull SchemaAttributeDefinitionFacade schAttrDefFacade,
            @NonNull TranslationFacade translationFacade
    ) {
        this.schAttrDefFacade = schAttrDefFacade;
        this.translationFacade = translationFacade;
    }

    @Override
    public List<SchemaAttributeDefinitionDTO> getDefinitions() {
        List<SchemaAttributeDefinitionEntity> definitions = schAttrDefFacade.getDefinitions();
        return EntityMappingUtils.mapSchemaAttributeDefinitions(definitions);
    }

    @Override
    public SchemaAttributeDefinitionDTO getDefinition(@NonNull Long id)
            throws SchemaAttributeDefinitionNotFoundException {
        SchemaAttributeDefinitionEntity definition = schAttrDefFacade.getDefinition(id);
        return EntityMappingUtils.map(definition);
    }

    @Override
    public SchemaAttributeDefinitionForm getDefinitionForEdit(@NonNull Long id)
            throws SchemaAttributeDefinitionNotFoundException {
        SchemaAttributeDefinitionForm form = new SchemaAttributeDefinitionForm();
        SchemaAttributeDefinitionEntity definition = schAttrDefFacade.getDefinition(id);
        Translations translations = translationFacade.getSchemaAttributeDefinitionTranslations(definition.getName());

        form.setName(definition.getName());
        form.setRpcName(definition.getPerunAttribute());
        form.setType(definition.getAttributeType());
        form.setMimeType(definition.getMimeType());
        form.addNameTranslations(translations.getNameTranslations());
        form.addDescriptionTranslations(translations.getDescTranslations());

        return form;
    }

    @Override
    public SchemaAttributeDefinitionDTO createDefinition(
            @NonNull SchemaAttributeDefinitionForm form
    ) {

        SchemaAttributeDefinitionEntity definition = schAttrDefFacade.createDefinition(form);

        translationFacade.createSchemaAttributeDefinitionTranslations(
                new Translations(form.getNameTranslations(), form.getDescriptionTranslations()),
                definition.getName()
        );

        return EntityMappingUtils.map(definition);
    }

    @Override
    @Transactional
    public SchemaAttributeDefinitionDTO updateDefinition(@NonNull Long id,
                                                         @NonNull SchemaAttributeDefinitionForm form)
            throws SchemaAttributeDefinitionNotFoundException {

        SchemaAttributeDefinitionEntity definition = schAttrDefFacade.getDefinition(id);
        String oldName = definition.getName();

        definition = schAttrDefFacade.updateDefinition(definition, form);

        translationFacade.updateSchemaAttributeDefinitionTranslations(
                new Translations(form.getNameTranslations(), form.getDescriptionTranslations()),
                definition.getName(),
                oldName
        );

        return EntityMappingUtils.map(definition);
    }

    @Override
    @Transactional
    public boolean deleteDefinition(@NonNull Long id)
            throws SchemaAttributeDefinitionNotFoundException
    {
        SchemaAttributeDefinitionEntity definition = schAttrDefFacade.getDefinition(id);
        translationFacade.deleteSchemaAttributeDefinitionTranslations(definition.getName());
        schAttrDefFacade.deleteDefinition(definition);
        return true;
    }

}
