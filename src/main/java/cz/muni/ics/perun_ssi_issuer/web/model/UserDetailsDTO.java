package cz.muni.ics.perun_ssi_issuer.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDetailsDTO {

    @NotBlank
    private String identifier;

    @NotBlank
    private String name;

    @NotBlank
    private String email;

}
