package cz.muni.ics.perun_ssi_issuer.data.perun.connector;


import cz.muni.ics.perun_ssi_issuer.data.perun.model.Attribute;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.User;
import lombok.NonNull;

import java.util.Collection;
import java.util.List;

public interface PerunConnector {

    String getInternalNameByRpcName(@NonNull String rpcName);

    User getUserByExtSourceNameAndExtLogin(@NonNull String extSourceName,
                                           @NonNull String extLogin);

    List<Attribute> getAttributes(@NonNull User user, Collection<String> attrNames);

}
