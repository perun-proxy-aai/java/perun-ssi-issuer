package cz.muni.ics.perun_ssi_issuer.common.exception;

public class NoUserIdentifierAvailableException extends Exception {

    public NoUserIdentifierAvailableException() {
        super();
    }

    public NoUserIdentifierAvailableException(String message) {
        super(message);
    }

    public NoUserIdentifierAvailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoUserIdentifierAvailableException(Throwable cause) {
        super(cause);
    }

    protected NoUserIdentifierAvailableException(String message, Throwable cause, boolean enableSuppression,
                                                 boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
