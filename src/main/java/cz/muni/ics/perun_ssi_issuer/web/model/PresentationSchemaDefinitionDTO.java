package cz.muni.ics.perun_ssi_issuer.web.model;

import cz.muni.ics.perun_ssi_issuer.middleware.facade.TranslationFacade;
import cz.muni.ics.perun_ssi_issuer.web.model.references.PresentationSchemaAttributeDefinitionReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class PresentationSchemaDefinitionDTO extends DefinitionLocalization {

    @NotNull
    private Long id;

    @NotNull
    private String schemaId;

    @NotBlank
    private String name;

    @NotNull
    @Builder.Default
    private Set<PresentationSchemaAttributeDefinitionReference> attributeDefinitionReferences = new HashSet<>();

    @Override
    public String i18nNameKey() {
        return TranslationFacade.getPresSchDefI18nNameKey(getName());
    }

    @Override
    public String i18nDescKey() {
        return TranslationFacade.getPresSchDefI18nDescKey(getName());
    }

}
