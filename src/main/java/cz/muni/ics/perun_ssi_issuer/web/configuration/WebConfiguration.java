package cz.muni.ics.perun_ssi_issuer.web.configuration;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.common.DBMessageSource;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.TranslationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_RESOURCES_ALL;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_RESOURCES_WEBJARS_ALL;

@Configuration
@EnableWebMvc
@Slf4j
public class WebConfiguration implements WebMvcConfigurer {

    // == CONSTANTS == //

    public static final String PARAM_LOCALE = "locale";
    private static final String[] RESOURCE_LOCATIONS_CLASSPATH = {
            "classpath:/META-INF/resources/",
            "classpath:/resources/",
            "classpath:/static/",
            "classpath:/public/"
    };
    private static final String RESOURCE_LOCATIONS_WEBJARS = "/webjars/";
    private static final String MESSAGES_FILE = "messages";

    // == CLASS FIELDS == //
    private final ApplicationProperties applicationProperties;

    private final TranslationRepository translationRepository;

    // == CONSTRUCTORS == //

    @Autowired
    public WebConfiguration(ApplicationProperties applicationProperties,
                            TranslationRepository translationRepository) {
        this.applicationProperties = applicationProperties;
        this.translationRepository = translationRepository;
    }

    // == PUBLIC METHODS == //

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if (StringUtils.hasText(applicationProperties.getStaticResourcesDirectory())) {
            registry.addResourceHandler("/local/**")
                    .addResourceLocations("file:" + applicationProperties.getStaticResourcesDirectory())
                    .resourceChain(false);
        }
        registry.addResourceHandler(PATH_RESOURCES_WEBJARS_ALL)
                .addResourceLocations(RESOURCE_LOCATIONS_WEBJARS)
                .resourceChain(false);
        registry.addResourceHandler(PATH_RESOURCES_ALL)
                .addResourceLocations(RESOURCE_LOCATIONS_CLASSPATH)
                .resourceChain(false);
    }



    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setUseTrailingSlashMatch(true);
    }

    // == PUBLIC METHODS - BEANS == //

    @Bean
    @Primary
    public MessageSource messageSource() {
        MessageSource defaultMessageSource = dbMessageSource();
        if (!StringUtils.hasText(applicationProperties.getLocalizationFilesDirectory())) {
            log.info("No path to filesystem i18n files provided, using only default" +
                    " messages configured in source...");
            return defaultMessageSource;
        } else {
            ReloadableResourceBundleMessageSource messageSource =
                    new ReloadableResourceBundleMessageSource();
            String path = "file:" + applicationProperties.getLocalizationFilesDirectory();
            if (!path.endsWith("/")) {
                path += '/';
            }
            path += MESSAGES_FILE;
            messageSource.setBasename(path);

            messageSource.setDefaultLocale(new Locale(applicationProperties.getDefaultLocale()));
            messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
            messageSource.setParentMessageSource(defaultMessageSource);
            messageSource.setUseCodeAsDefaultMessage(false);

            log.info("I18n files from path '{}'. Using default files as fallback...", path);
            return messageSource;
        }
    }

    @Bean("defaultMessageSource")
    public MessageSource defaultMessageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename(MESSAGES_FILE);
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setDefaultLocale(new Locale(applicationProperties.getDefaultLocale()));
        messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());

        messageSource.setParentMessageSource(null);
        return messageSource;
    }

    @Bean("dbMessageSource")
    public MessageSource dbMessageSource() {
        DBMessageSource messageSource = new DBMessageSource(translationRepository);
        messageSource.setUseCodeAsDefaultMessage(false);
        messageSource.setParentMessageSource(defaultMessageSource());
        return messageSource;
    }

    @Bean
    public LocaleResolver localeResolver() {
        CookieLocaleResolver slr = new CookieLocaleResolver();
        slr.setDefaultLocale(new Locale(applicationProperties.getDefaultLocale()));
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName(PARAM_LOCALE);
        return lci;
    }

    @Bean
    @Primary
    @Autowired
    public ITemplateResolver templateResolver(
            @Qualifier("defaultTemplateResolver") SpringResourceTemplateResolver defaultTemplateResolver
    ) {
        if (!StringUtils.hasText(applicationProperties.getTemplateFilesDirectory())) {
            log.info("No path to filesystem template files provided, using only default" +
                    " messages configured in source...");
            return defaultTemplateResolver;
        } else {
            FileTemplateResolver filesystemTemplateResolver = new FileTemplateResolver();
            filesystemTemplateResolver.setPrefix(applicationProperties.getTemplateFilesDirectory());
            filesystemTemplateResolver.setSuffix(".html");
            filesystemTemplateResolver.setTemplateMode(TemplateMode.HTML);
            filesystemTemplateResolver.setCharacterEncoding("UTF-8");
            filesystemTemplateResolver.setOrder(0);
            filesystemTemplateResolver.setCheckExistence(true);

            defaultTemplateResolver.setOrder(1);

            return filesystemTemplateResolver;
        }
    }

}
